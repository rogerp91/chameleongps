# ChameleonGPS #

Android - Code - Screenshot

------

# Descripción

ChameleonGPS es una aplicación de localización que sirve para ver la ubicación de flota de unidades como vehículos, camiones y motos, también permite el encendido y apagado de dichas unidades, se pueden ver mapas, recibir notificaciones de alarmas y utilizar el celular como equipo de rastreo GPS.
Tecnologías Utilizadas: ButterKnife, Retrofit, GSON, Otto, Dagger, CircleImageView, Google Maps, Google Play Location, UniversalImageLoader

---

# Descripción

![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503362747/android-app-chameleongps-unidad_fifmwc.png  | 100x100 )
![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503362748/android-app-chameleongps-alertas_gbsh2b.png  | 100x100 )
![](http://res.cloudinary.com/dyamfdx4a/image/upload/v1503362747/android-app-chameleongps-unidad_fifmwc.png  | 100x100 )