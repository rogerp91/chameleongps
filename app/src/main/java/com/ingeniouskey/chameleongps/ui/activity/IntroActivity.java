package com.ingeniouskey.chameleongps.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.ingeniouskey.chameleongps.ui.fragment.SampleSlide;
import com.ingeniouskey.chameleongps.utils.Constants;
import com.ingeniouskey.chameleongps.utils.Prefs;
import com.ingeniouskey.chameleongps.R;
/**
 * Created by Roger Patiño on 02/02/2016.
 */
public class IntroActivity extends AppIntro {

    @Override
    public void init(Bundle savedInstanceState) {
        addSlide(SampleSlide.newInstance(R.layout.intro_app_1));
        addSlide(SampleSlide.newInstance(R.layout.intro_app_2));
        addSlide(SampleSlide.newInstance(R.layout.intro_app_3));
        addSlide(SampleSlide.newInstance(R.layout.intro_app_4));
        addSlide(SampleSlide.newInstance(R.layout.intro_app_5));
    }

    @Override
    public void onSkipPressed() {
        loadMainActivity();
    }

    @Override
    public void onNextPressed() {

    }

    @Override
    public void onDonePressed() {
        loadMainActivity();
    }

    @Override
    public void onSlideChanged() {

    }

    //goto login
    private void loadMainActivity() {
        Prefs.putBoolean(Constants.INTRODUTION, true);
        startActivity(new Intent(this, LoginActivity.class));
    }
}