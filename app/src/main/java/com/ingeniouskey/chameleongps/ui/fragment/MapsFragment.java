package com.ingeniouskey.chameleongps.ui.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.components.ProgressWheel;
import com.ingeniouskey.chameleongps.di.modules.SnackbarModule;
import com.ingeniouskey.chameleongps.mvp.model.Unit;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.MapsPresenter;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.ShowMsgPresenter;
import com.ingeniouskey.chameleongps.mvp.view.MapsView;
import com.ingeniouskey.chameleongps.utils.Functions;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Roger Patiño on 28/12/2015.
 */
public class MapsFragment extends BaseFragment implements MapsView, OnMapReadyCallback {

    public static final String ARG_SECTION_TITLE = "section_number";

    public static MapsFragment newInstance(String sectionTitle) {
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        return fragment;
    }

    private View view;
    //container fragment view que necesita error
    @Bind(R.id.maps_fragment)
    FrameLayout frameLayout;

    //google maps
    private GoogleMap googleMap;

    //alert msj
    @Bind(R.id.progress)
    ProgressWheel mProgressView;

    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.layout_no_content)
    RelativeLayout layout_noContent;

    @Bind(R.id.layout_connection)
    RelativeLayout layout_Connection;
    private SupportMapFragment mapFragment;
    private GoogleMap map;

    @Inject
    ShowMsgPresenter msgPresenter;

    @Inject
    MapsPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_maps, container, false);
        ButterKnife.bind(this, view);
        if (isAdded()) {
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            if (mapFragment == null && getActivity() != null) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                mapFragment = SupportMapFragment.newInstance();
                fragmentTransaction.replace(R.id.map, mapFragment).commit();
            }
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.setView(this);
        presenter.getAlertUnit();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng sydney = new LatLng(-33.867, 151.206);
        map = googleMap;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Functions.checkPlayServices()) {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public List<Object> getModules() {// module por actividad
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void showMsg(String mgs, int color) {
        msgPresenter.showMsgShort(mgs, color);
    }

    @Override
    public void displayError() {
        msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getActivity().getApplicationContext(), R.color.alert));
    }

    @Override
    public void displayErrorConnection() {
        msgPresenter.showMsgShort(getResources().getString(R.string.no_connection2), ContextCompat.getColor(getActivity().getApplicationContext(), R.color.alert));
    }

    @Override
    public void showViewProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void showViewRefresh(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        swipeRefreshLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        swipeRefreshLayout.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                swipeRefreshLayout.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    @Override
    public void displayLayoutConnection(final boolean connection) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
        layout_Connection.animate().setDuration(shortAnimTime).alpha(connection ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void displayLayoutNocontent(final boolean content) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
        layout_noContent.animate().setDuration(shortAnimTime).alpha(content ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void showMapsAndMarker(boolean show) {

    }

    @Override
    public void displayApdateMaps(List<Unit> units) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        List<MarkerOptions> markerOptionses = new ArrayList<>();
        for (int x = 0; x < units.size(); x++) {
            MarkerOptions marker = new MarkerOptions().position(new LatLng(units.get(x).getLatitud(), units.get(x).getLongitud())).title(units.get(x).getNombre());
            markerOptionses.add(marker);
            marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
            map.addMarker(marker);
            map.setMyLocationEnabled(true);
            //bounds = new LatLngBounds.Builder().include(new LatLng(cn.getLatitude(), cn.getLongitude())).build();
        }

        for (int i = 0; i < markerOptionses.size(); i++) {
            map.addMarker(markerOptionses.get(i));
            builder.include(markerOptionses.get(i).getPosition());
        }

        LatLngBounds bounds = builder.build();
        int padding = 40; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        map.animateCamera(cu);

    }

    @OnClick(R.id.text_try_again_content)
    public void TryAgainContent() {
        presenter.setView(this);
        presenter.getAlertUnit();
    }

    @OnClick(R.id.text_try_again)
    public void TryAgain() {
        presenter.setView(this);
        presenter.getAlertUnit();
    }

    @OnClick(R.id.reload)
    public void TryReload() {
        presenter.getAlertUnitRefresh();
    }

    @Nullable
    @Override
    public View getView() {
        return frameLayout;
    }

    @Override
    public boolean getAdded() {
        return isAdded();
    }
}