package com.ingeniouskey.chameleongps.ui.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.devspark.robototextview.widget.RobotoTextView;
import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.components.ProgressWheel;
import com.ingeniouskey.chameleongps.di.modules.SnackbarModule;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.LoginPresenter;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.ShowMsgPresenter;
import com.ingeniouskey.chameleongps.mvp.view.LoginView;
import com.ingeniouskey.chameleongps.utils.Functions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView {

    protected static String TAG = LoginActivity.class.getSimpleName();
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    //layout para snackbar
    @Bind(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @Bind(R.id.scroll_form)
    View mForm;
    @Bind(R.id.progress)
    ProgressWheel mProgress;
    @Bind(R.id.text_lat)
    RobotoTextView mTextLat;

    //input
    @Bind(R.id.user)
    EditText inputEmail;
    @Bind(R.id.password)
    EditText inputPassword;
    @Bind(R.id.name)
    TextView names;
    private static boolean PERMISION = false;

    @Inject
    ShowMsgPresenter msgPresenter;
    @Inject
    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @Override
    protected List<Object> getModules() {
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
        if (!Functions.checkPlayServices()) {
            //showDialogGooglePlay();
        }
        if (!Functions.checkPlayServices()) {
            Log.d(TAG, "Play Service no install");
            //finish();
        } else {
            Log.d(TAG, "Play Service install");
        }

        if (Functions.isMarshmallow()) {
            Log.d(TAG, "Android: Marshmallow");
        }
        if (Functions.isMarshmallow()) {
            Log.d(TAG, "Android: Marshmallow");
            multiplePermmision();
        }
    }

    @Override
    public View getView() {
        return relativeLayout;
    }

    @Override
    public void displayInfoProfile() {
        msgPresenter.showMsgShort(getResources().getString(R.string.error_profile), ContextCompat.getColor(getApplicationContext(), R.color.alert));
    }

    private void multiplePermmision() {
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Memory external");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    }
                });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LoginActivity.this).setMessage(message).setPositiveButton("OK", okListener).setNegativeButton("Cancel", null).create().show();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:
                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    PERMISION = true;
                } else {
                    msgPresenter.showMsgShort("Some Permission is Denied", ContextCompat.getColor(getApplicationContext(), R.color.alert));
                    startActivity(new Intent(Settings.ACTION_SETTINGS));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void showView(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mForm.setVisibility(show ? View.GONE : View.VISIBLE);
        mForm.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mForm.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });
        names.setVisibility(show ? View.GONE : View.VISIBLE);
        names.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                names.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mTextLat.setVisibility(show ? View.VISIBLE : View.GONE);
        mTextLat.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mTextLat.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgress.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void displayMsg(String msg, int color) {
        msgPresenter.showMsgShort(msg, color);
    }

    @Override
    public void gotoMain(Intent i) {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    @Override
    public void displayErrorData() {
        msgPresenter.showMsgShort(getResources().getString(R.string.validate), ContextCompat.getColor(getApplicationContext(), R.color.alert));
    }

    @Override
    public void displayError() {
        msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getApplicationContext(), R.color.alert));
    }

    @Override
    public void displayErrorConnection() {
        msgPresenter.showMsgShort(getResources().getString(R.string.no_connection2), ContextCompat.getColor(getApplicationContext(), R.color.alert));
    }

    @Override
    public void displayInfoUser() {
        msgPresenter.showMsgShort(getResources().getString(R.string.not_empty_user), ContextCompat.getColor(getApplicationContext(), R.color.info));
    }

    @Override
    public void displayInfoUserSize() {
        msgPresenter.showMsgShort(getResources().getString(R.string.min_length_user), ContextCompat.getColor(getApplicationContext(), R.color.info));
    }

    @Override
    public void displayInfoPassword() {
        msgPresenter.showMsgShort(getResources().getString(R.string.not_empty_password), ContextCompat.getColor(getApplicationContext(), R.color.info));
    }

    @Override
    public void displayInfoPasswordSize() {
        msgPresenter.showMsgShort(getResources().getString(R.string.min_length_password), ContextCompat.getColor(getApplicationContext(), R.color.info));
    }

    //----------------------------------------------------------------------------------------------
    @OnClick(R.id.btnEnter)
    public void OnClickLogin() {
        if (Functions.isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION) && Functions.isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            presenter.validateCredentials(inputEmail.getText().toString(), inputPassword.getText().toString());
        } else {
            msgPresenter.showMsgShort(getResources().getString(R.string.aprob_permision), ContextCompat.getColor(getApplicationContext(), R.color.alert));
            Snackbar snackbar = Snackbar.make(relativeLayout, getResources().getString(R.string.aprob_permision), Snackbar.LENGTH_INDEFINITE);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.alert));
            snackbar.setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //startActivity(new Intent(Settings.ACTION_APPLICATION_SETTINGS));
                    Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.setData(Uri.parse("package:" + getPackageName()));
                    startActivity(i);
                }
            }).show();
        }
    }
}