package com.ingeniouskey.chameleongps.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.components.ProgressWheel;
import com.ingeniouskey.chameleongps.di.modules.SnackbarModule;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.AlertRepository;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.Repository;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.UnitRepository;
import com.ingeniouskey.chameleongps.mvp.model.ProfileItem;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.ShowMsgPresenter;
import com.ingeniouskey.chameleongps.mvp.view.MainView;
import com.ingeniouskey.chameleongps.ui.fragment.AlertFragment;
import com.ingeniouskey.chameleongps.ui.fragment.MapsFragment;
import com.ingeniouskey.chameleongps.ui.fragment.UnitFragment;
import com.ingeniouskey.chameleongps.utils.Constants;
import com.ingeniouskey.chameleongps.utils.Functions;
import com.ingeniouskey.chameleongps.utils.Prefs;
import com.ingeniouskey.chameleongps.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements MainView {

    protected static String TAG = MainActivity.class.getSimpleName();
    public static Activity GET_ACTIVITY;
    public static View VIEW_ACTIVITY;

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    MaterialDialog dialoglogOut = null;
    LogOut logOut;//salir

    //drawer layout
    private String drawerTitle;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.nav_view)
    NavigationView navigationView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.contentLayout)
    LinearLayout layout;

    //perfil
    private ProfileItem findProfileItem = null;//va a obetnet los id del header del drawer
    private AppCompatButton btnHeader = null;

    @Inject
    ShowMsgPresenter msgPresenter;

    @Inject
    Repository repository;

    UnitRepository unitRepository;
    AlertRepository alertRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean login = Prefs.getBoolean(Constants.LOGIN, false);
        boolean intro = Prefs.getBoolean(Constants.INTRODUTION, false);
        if (!intro && !login) {
            finish();
            startActivity(new Intent(this, IntroActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        } else {
            if (intro && !login) {
                finish();
                startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_FullScreen);//aplicar estilo
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        GET_ACTIVITY = MainActivity.this;

        setToolbar();
        VIEW_ACTIVITY = layout;

        unitRepository = repository.getUnit();
        alertRepository = repository.getAlert();

        //drawer
        if (navigationView != null) {//set nav
            setupDrawerContent(navigationView);
        }
        drawerTitle = getResources().getString(R.string.home_item);
        if (savedInstanceState == null) {
            selectItem(drawerTitle);
        }

        //inflar el header de los datos de usuario
        View headerNav = navigationView.inflateHeaderView(R.layout.nav_header);//inflar el header del drawer
        btnHeader = (AppCompatButton) headerNav.findViewById(R.id.errorHeader);
        findProfileItem = new ProfileItem();//asignar los vista al objeto
        findProfileItem.setUsername((TextView) headerNav.findViewById(R.id.username));
        findProfileItem.setEmail((TextView) headerNav.findViewById(R.id.email));
        findProfileItem.setCircleImageView((CircleImageView) headerNav.findViewById(R.id.circle_image));
        findProfileItem.setProgressWheel((ProgressWheel) headerNav.findViewById(R.id.progress));
        //if (!Prefs.getBoolean(Constants.SUCESS_PROFILE, false))
        if (login) {//verificar si ha entrador por primera vez
            //ProfileService.actionStart();
            //headerShow(true, findProfileItem);
            showProfile(findProfileItem);
        }

        if (login) {
            //adn fragment
            setFragment(getResources().getString(R.string.maps_item));
        }

    }

    @Override
    protected List<Object> getModules() {
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!Prefs.getBoolean(Constants.SESSION, false)) {
            Prefs.putBoolean(Constants.LOGIN, false);
            finish();
            startActivity(new Intent(UIApp.getContext, IntroActivity.class));
        }
        if (!Functions.checkPlayServices()) {
            Log.d(TAG, "Play Service no install");
            //finish();
        } else {
            Log.d(TAG, "Play Service install");
        }
        if (Functions.isMarshmallow()) {
            Log.d(TAG, "Android: Marshmallow");
            multiplePermmision();
        }
    }

    private void multiplePermmision() {
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Memory external");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    }
                });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this).setMessage(message).
                setPositiveButton("OK", okListener).
                setNeutralButton("Permision", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.setData(Uri.parse("package:" + getPackageName()));
                        startActivity(i);
                    }
                }).
                setNegativeButton("Cancel", null).create().show();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:
                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                } else {
                    msgPresenter.showMsgShort("Some Permission is Denied", ContextCompat.getColor(getApplicationContext(), R.color.alert));
                }
                break;
        }
    }


    @Override
    public View getView() {
        return layout;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
            if (unitRepository.countUnit() != 0) {
                getMenuInflater().inflate(R.menu.main, menu);
                MenuItem item = menu.findItem(R.id.action_notifications);
                LayerDrawable icon = (LayerDrawable) item.getIcon();
                Utils.setBadgeCount(icon, unitRepository.countUnit());
                return true;
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //----------------------------------------------------------------------------------------------

    //show perfil
    private void showProfile(ProfileItem profileItem) {
        profileItem.getUsername().setText(Prefs.getString(Constants.USUARIO, ""));
        profileItem.getEmail().setText(Prefs.getString(Constants.CORREO, ""));
        profileItem.getUsername().setVisibility(View.VISIBLE);
        profileItem.getEmail().setVisibility(View.VISIBLE);
        profileItem.getCircleImageView().setVisibility(View.VISIBLE);
        profileItem.getEmail().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(UIApp.getContext, SettingsActivity.class));
            }
        });

    }

    private void logOut() {
        dialoglogOut = new MaterialDialog.Builder(this).title(R.string.log_out_title).content(R.string.log_out_content).progress(true, 0).show();
        logOut = new LogOut();
        logOut.execute((Void) null);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.mipmap.ic_menu_white_24dp);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private class LogOut extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Prefs.putBoolean(Constants.SESSION, false);
            Prefs.putBoolean(Constants.LOGIN, false);
            Prefs.putString(Constants.USUARIO, "");
            Prefs.putString(Constants.CORREO, "");
            Prefs.putString(Constants.TOKEN, "");
            return true;
        }


        @Override
        protected void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool) {
                alertRepository.clear();
                unitRepository.clear();
                logOut.isCancelled();
                dialoglogOut.dismiss();
                finish();
                startActivity(new Intent(UIApp.getContext, LoginActivity.class));
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {

                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        String title = menuItem.getTitle().toString();
                        switch (menuItem.getItemId()) {
                            case R.id.nav_maps:
                                menuItem.setChecked(true);
                                setFragment(0, title);
                                selectItem(title);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.nav_unit:
                                menuItem.setChecked(true);
                                setFragment(1, title);
                                selectItem(title);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.nav_alert:
                                menuItem.setChecked(true);
                                setFragment(2, title);
                                selectItem(title);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;
                            case R.id.nav_log_out:
                                logOut();
                                return true;
                        }
                        return true;
                    }
                }
        );
    }

    private void setFragment(int position, String name) {
        switch (position) {
            case 0:

                 Log.d(TAG, "setFragment");
                 getSupportFragmentManager().beginTransaction().replace(R.id.main_content, MapsFragment.newInstance(name))
                 .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                 .addToBackStack(null)
                 .commit();

                break;
            case 1:
                Log.d(TAG, "setFragment");
                getSupportFragmentManager().beginTransaction().replace(R.id.main_content, UnitFragment.newInstance(name))
                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                //.addToBackStack(null)
                        .commit();

                break;
            case 2:
                Log.d(TAG, "setFragment");
                getSupportFragmentManager().beginTransaction().replace(R.id.main_content, AlertFragment.newInstance(name))
                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                //.addToBackStack(null)
                        .commit();

                break;

        }
    }

    //fragment que se inicia primero
    private void setFragment(String title) {

         getSupportFragmentManager()
         .beginTransaction()
         .replace(R.id.main_content, MapsFragment.newInstance(title))
         .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
         //.addToBackStack(null)
         .commit();
        selectItem(title);
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void selectItem(String title) {//tiitle
        drawerLayout.closeDrawers(); // Cerrar drawer
        setTitle(title); // Setear título actual
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        Log.d(TAG, Integer.toString(fm.getBackStackEntryCount()));
        if (fm.getBackStackEntryCount() > 0) {
            Log.i(TAG, "popping backstack");
            fm.popBackStack();
        } else {
            Log.i(TAG, "nothing on backstack, calling super");
            super.onBackPressed();
        }
    }
}