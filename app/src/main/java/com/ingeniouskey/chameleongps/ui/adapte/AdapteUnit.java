package com.ingeniouskey.chameleongps.ui.adapte;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.domain.repository.datasource.api.HttpRestClient;
import com.ingeniouskey.chameleongps.mvp.model.AlertParcel;
import com.ingeniouskey.chameleongps.mvp.model.Command;
import com.ingeniouskey.chameleongps.mvp.model.Unit;
import com.ingeniouskey.chameleongps.ui.activity.MainActivity;
import com.ingeniouskey.chameleongps.ui.activity.MapsDetailActivity;
import com.ingeniouskey.chameleongps.ui.fragment.AlertFragment;
import com.ingeniouskey.chameleongps.utils.Constants;
import com.ingeniouskey.chameleongps.utils.Functions;
import com.ingeniouskey.chameleongps.utils.Prefs;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 10/02/2016.
 */
public class AdapteUnit extends RecyclerView.Adapter<AdapteUnit.ViewHolder> {

    protected static String TAG = AdapteAlert.class.getSimpleName();

    public List<Unit> list = null;
    private Context context;

    public AdapteUnit(List<Unit> unitList) {
        this.list = unitList;
        context = UIApp.getContext;
    }

    public interface OnItemClickListener {
        void onItemClick(RecyclerView.ViewHolder item, int position);
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_unit, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Unit unit = list.get(position);
        holder.indetificador.setText(unit.getIdentificador());
        holder.available.setText(Integer.toString(unit.getId()));
        holder.type.setText(unit.getTipo());

        final String name = unit.getNombre();
        final String tipo = unit.getTipo();
        final int id_unit = unit.getId();

        //color icon
        holder.icLocate.setColorFilter(ContextCompat.getColor(context, android.R.color.secondary_text_dark));
        //holder.icHistory.setColorFilter(ContextCompat.getColor(context, android.R.color.secondary_text_dark));
        // holder.icService.setColorFilter(ContextCompat.getColor(context, android.R.color.secondary_text_dark));
        //holder.icPerimeters.setColorFilter(ContextCompat.getColor(context, android.R.color.secondary_text_dark));
        holder.icCommands.setColorFilter(ContextCompat.getColor(context, android.R.color.secondary_text_dark));

        final ImageView icLocateImageView = holder.icLocate;

        holder.icLocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UIApp.getContext, MapsDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.NAME_ACTIVITY, "Unit");
                intent.putExtra(Constants.ALERT, changeUnitToAlert(unit));
                UIApp.getContext.startActivity(intent);
                MainActivity.GET_ACTIVITY.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });
        /**
         holder.icHistory.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

        }
        });
         holder.icService.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

        }
        });
         holder.icPerimeters.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {

        }
        });
         */

        final AppCompatButton compatButtonApagar = null;
        AppCompatButton compatButtonEncender = null;
        holder.icCommands.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //intancias
                AppCompatButton compatButtonEncender = null;
                AppCompatButton compatButtonApagar = null;

                //dialog
                final MaterialDialog dialog = new MaterialDialog.Builder(MainActivity.GET_ACTIVITY).
                        title(name + " - " + tipo).
                        customView(R.layout.dialog_command, true).
                        negativeText(android.R.string.cancel).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).build();

                //intancias de ñps dialogos
                compatButtonApagar = (AppCompatButton) dialog.getCustomView().findViewById(R.id.apagar);
                compatButtonEncender = (AppCompatButton) dialog.getCustomView().findViewById(R.id.encender);


                //peticiones
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
                Gson gson = gsonBuilder.create();
                final String token = Prefs.getString(Constants.TOKEN, "");
                OkHttpClient okHttpClient = new OkHttpClient();
                Retrofit retrofit = new Retrofit.Builder().baseUrl(HttpRestClient.BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).client(okHttpClient).build();
                final HttpRestClient httpRestClient = retrofit.create(HttpRestClient.class);

                //dialog btn
                compatButtonApagar.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        final MaterialDialog.Builder builder = new MaterialDialog.Builder(MainActivity.GET_ACTIVITY).title(context.getResources().getString(R.string.apagar) + " " + name).content(R.string.log_out_content).progress(true, 0).progressIndeterminateStyle(true);
                        final MaterialDialog dialogApg = builder.build();
                        dialogApg.show();
                        Call<Void> voidCall = httpRestClient.performAlertCommands(HttpRestClient.PREFIX + token, new Command(id_unit, "unLockOn"));
                        if (Functions.isOnline()) {
                            voidCall.enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Response<Void> response, Retrofit retrofit) {
                                    System.out.println("Adapte : " + response.code());
                                    switch (response.code()) {
                                        case 200:
                                            dialogApg.dismiss();
                                            dialog.dismiss();
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            showMsg("Equipo apagado.", ContextCompat.getColor(UIApp.getContext, R.color.success));
                                            break;
                                        case 204:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 400:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 401:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 403:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 404:
                                            dialogApg.dismiss();
                                            dialog.dismiss();
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            showMsg("Unidad no encontrada.", ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                            break;
                                        case 500:
                                            dialogApg.dismiss();
                                            dialog.dismiss();
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            showMsg("Terminal no encontrado.", ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                            break;
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    dialogApg.dismiss();
                                    Snackbar snackbar = Snackbar.make(MainActivity.VIEW_ACTIVITY, UIApp.getContext.getResources().getText(R.string.delete_alert_error_occurred), Snackbar.LENGTH_LONG);
                                    View snackBarView = snackbar.getView();
                                    snackBarView.setBackgroundColor(ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                    snackbar.show();
                                }
                            });
                        }
                    }
                });

                compatButtonEncender.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        final MaterialDialog.Builder builder = new MaterialDialog.Builder(MainActivity.GET_ACTIVITY).title(context.getResources().getString(R.string.encender) + " " + name).content(R.string.log_out_content).progress(true, 0).progressIndeterminateStyle(true);
                        final MaterialDialog dialogEnc = builder.build();
                        dialogEnc.show();
                        Call<Void> voidCall = httpRestClient.performAlertCommands(HttpRestClient.PREFIX + token, new Command(id_unit, "lockOn"));
                        if (Functions.isOnline()) {
                            voidCall.enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Response<Void> response, Retrofit retrofit) {
                                    System.out.println("Adapte : " + response.code());
                                    switch (response.code()) {
                                        case 200:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            dialogEnc.dismiss();
                                            dialog.dismiss();
                                            showMsg("Equipo enecendido.", ContextCompat.getColor(UIApp.getContext, R.color.success));
                                            break;
                                        case 204:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 400:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 401:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 403:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 404:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            dialogEnc.dismiss();
                                            dialog.dismiss();
                                            showMsg("Unidad no encontrada.", ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                            break;
                                        case 500:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            dialogEnc.dismiss();
                                            dialog.dismiss();
                                            showMsg("Terminal no encontrado.", ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                            break;
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    dialogEnc.dismiss();
                                    Snackbar snackbar = Snackbar.make(MainActivity.VIEW_ACTIVITY, UIApp.getContext.getResources().getText(R.string.delete_alert_error_occurred), Snackbar.LENGTH_LONG);
                                    View snackBarView = snackbar.getView();
                                    snackBarView.setBackgroundColor(ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                    snackbar.show();
                                }
                            });
                        }
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Campos respectivos de un item
        public TextView indetificador;
        public TextView available;
        public TextView type;

        public ImageView icLocate;
        //public ImageView icHistory;
        //public ImageView icService;
        //public ImageView icPerimeters;
        public ImageView icCommands;

        private AdapteUnit father = null;

        public ViewHolder(View view, AdapteUnit father) {
            super(view);
            view.setOnClickListener(this);
            this.father = father;
            indetificador = (TextView) view.findViewById(R.id.identificador);
            available = (TextView) view.findViewById(R.id.available);
            type = (TextView) view.findViewById(R.id.type);

            icLocate = (ImageView) view.findViewById(R.id.ic_locate);
            //icHistory = (ImageView) view.findViewById(R.id.ic_history);
            //icService = (ImageView) view.findViewById(R.id.ic_service);
            //icPerimeters = (ImageView) view.findViewById(R.id.ic_perimeters);
            icCommands = (ImageView) view.findViewById(R.id.ic_commands);

        }

        @Override
        public void onClick(View v) {
            final AdapteUnit.OnItemClickListener listener = father.getOnItemClickListener();
            if (listener != null) {
                listener.onItemClick(this, getAdapterPosition());
            }
        }
    }

    //mostrar mensaje
    public void showMsg(String msg, int color) {
        Snackbar snackbar = Snackbar.make(MainActivity.VIEW_ACTIVITY, msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(color);
        snackbar.show();
    }

    public void apagar() {

    }

    public void encender() {

    }

    //cambiar de un objeto realm a un objeto parcelable
    private AlertParcel changeUnitToAlert(final Unit unit) {
        AlertParcel alertParcel = new AlertParcel();
        alertParcel.setId(unit.getId());
        alertParcel.setTipo(unit.getTipo());
        alertParcel.setLatitud(unit.getLatitud());
        alertParcel.setLongitud(unit.getLongitud());
        alertParcel.setIdentificador(unit.getIdentificador());
        return alertParcel;
    }

}