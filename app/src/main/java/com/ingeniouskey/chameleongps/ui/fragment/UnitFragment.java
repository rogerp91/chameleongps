package com.ingeniouskey.chameleongps.ui.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.components.ProgressWheel;
import com.ingeniouskey.chameleongps.di.modules.SnackbarModule;
import com.ingeniouskey.chameleongps.mvp.model.Unit;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.ShowMsgPresenter;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.UnitPresenter;
import com.ingeniouskey.chameleongps.mvp.view.UnitView;
import com.ingeniouskey.chameleongps.ui.adapte.AdapteUnit;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Roger Patiño on 10/02/2016.
 */
public class UnitFragment extends BaseFragment implements UnitView, AdapteUnit.OnItemClickListener {

    public static final String UnitFragment = "UnitFragment";
    public static final String ARG_SECTION_TITLE = "section_number";

    public static UnitFragment newInstance(String sectionTitle) {
        UnitFragment fragment = new UnitFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }

    //var
    private View view = null;
    @Bind(R.id.unit_fragment)
    FrameLayout frameLayout;

    @Bind(R.id.list_recycler_unit)
    RecyclerView recycler;
    private LinearLayoutManager linearManager;
    private AdapteUnit adaptador;
    //alert msj
    @Bind(R.id.progress)
    ProgressWheel mProgressView;

    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.layout_no_content)
    RelativeLayout layout_noContent;

    @Bind(R.id.layout_connection)
    RelativeLayout layout_Connection;

    @Inject
    ShowMsgPresenter msgPresenter;

    @Inject
    UnitPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_unit, container, false);
        ButterKnife.bind(this, view);
        //list
        recycler.setHasFixedSize(true);
        linearManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(linearManager);

        // SwipeRefresh
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getUnitListRefresh();
            }
        });

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.setView(this);
        presenter.getUnitList();
    }

    @Override
    public List<Object> getModules() {// module por actividad
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }

    @Override
    public void showView(boolean show) {

    }

    @Override
    public void showMsg(String mgs, int color) {
        msgPresenter.showMsgShort(mgs, color);
    }

    @Override
    public void displayError() {
        msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getActivity().getApplicationContext(), R.color.alert));
    }

    @Override
    public void displayErrorConnection() {
        msgPresenter.showMsgShort(getResources().getString(R.string.no_connection2), ContextCompat.getColor(getActivity().getApplicationContext(), R.color.alert));
    }

    @Override
    public void adapteList(List<Unit> list) {
        adaptador = new AdapteUnit(list);
        adaptador.setHasStableIds(true);
        adaptador.setOnItemClickListener(this);
        recycler.setAdapter(adaptador);
    }

    @Override
    public void gotoMain(Intent i) {

    }

    @Override
    public void showViewProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void showViewRefresh(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        swipeRefreshLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        swipeRefreshLayout.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                swipeRefreshLayout.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    @Override
    public void displayLayoutConnection(final boolean connection) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
        layout_Connection.animate().setDuration(shortAnimTime).alpha(connection ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void displayLayoutNocontent(final boolean content) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
        layout_noContent.animate().setDuration(shortAnimTime).alpha(content ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public boolean getAdded() {
        return isAdded();
    }

    @Override
    public void onItemClick(RecyclerView.ViewHolder item, int position) {

    }

    @OnClick(R.id.text_try_again_content)
    public void error() {
        presenter.setView(this);
        presenter.getUnitList();
    }
}