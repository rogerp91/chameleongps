package com.ingeniouskey.chameleongps.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.mvp.model.AlertParcel;
import com.ingeniouskey.chameleongps.utils.Constants;
import com.ingeniouskey.chameleongps.utils.Functions;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MapsDetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.subtitle)
    TextView subtitle;
    @Bind(R.id.date)
    TextView date;

    private GoogleMap googleMap;

    @Bind(R.id.layout_no_content)
    RelativeLayout relativeLayout;

    AlertParcel alert = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_detail);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (Functions.isLolipop()) {
            Fade fade = new Fade();
            fade.setDuration(1000);
            getWindow().setEnterTransition(fade);
        }

        if (getIntent() != null) {
            this.alert = getIntent().getParcelableExtra(Constants.ALERT);
            getSupportActionBar().setTitle(getIntent().getExtras().getString(Constants.NAME_ACTIVITY));
        }

    }

    @Override
    protected void onResume() {
        if (Functions.checkPlayServices()) {

        }
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Functions.checkPlayServices()) {
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (Functions.checkPlayServices()) {

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Functions.checkPlayServices()) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MarkerOptions marker = new MarkerOptions().position(new LatLng(alert.getLatitud(), alert.getLongitud())).title(alert.getIdentificador());
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
        googleMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(alert.getLatitud(), alert.getLongitud())).zoom(14f).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        title.setText(alert.getIdentificador());
        subtitle.setText(alert.getTipo());
        date.setText(alert.getFecha_actual());
    }

}