package com.ingeniouskey.chameleongps.ui.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.components.ProgressWheel;
import com.ingeniouskey.chameleongps.di.modules.SnackbarModule;
import com.ingeniouskey.chameleongps.mvp.model.Alert;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.AlertPresenter;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.ShowMsgPresenter;
import com.ingeniouskey.chameleongps.mvp.view.AlertView;
import com.ingeniouskey.chameleongps.ui.adapte.AdapteAlert;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Roger Patiño on 05/02/2016.
 */
public class AlertFragment extends BaseFragment implements AlertView, AdapteAlert.OnItemClickListener {

    public static final String AlertFragmentName = "AlertFragment";
    public static Bundle savedInstanceStateStatic;
    public static final String ARG_SECTION_TITLE = "section_number";
    public static Activity FRAGMENT_ALERT = null;

    public static AlertFragment newInstance(String sectionTitle) {
        AlertFragment fragment = new AlertFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }


    //var
    private View view = null;
    @Bind(R.id.alert_fragment)
    FrameLayout frameLayout;

    @Bind(R.id.list_recycler)
    RecyclerView recycler;
    private LinearLayoutManager linearManager;
    private AdapteAlert adaptador;
    //alert msj
    @Bind(R.id.progress)
    ProgressWheel mProgressView;

    @Bind(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.layout_no_content)
    RelativeLayout layout_noContent;

    @Bind(R.id.layout_connection)
    RelativeLayout layout_Connection;

    @Inject
    ShowMsgPresenter msgPresenter;

    @Inject
    AlertPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_alert, container, false);
        ButterKnife.bind(this, view);

        savedInstanceStateStatic = savedInstanceState;
        FRAGMENT_ALERT = getActivity();

        //list
        recycler.setHasFixedSize(true);
        linearManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(linearManager);

        // SwipeRefresh
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity().getApplicationContext(), R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getAlertListRefresh();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.setView(this);
        presenter.getAlertList();
    }

    @Override
    public List<Object> getModules() {// module por actividad
        LinkedList<Object> modules = new LinkedList<>();
        modules.add(new SnackbarModule(this));
        return modules;
    }

    @Override
    public void showView(boolean show) {

    }

    @Override
    public void showMsg(String mgs, int color) {
        msgPresenter.showMsgShort(mgs, color);
    }

    @Override
    public void displayError() {
        msgPresenter.showMsgShort(getResources().getString(R.string.error_occurred), ContextCompat.getColor(getActivity().getApplicationContext(), R.color.alert));
    }

    @Override
    public void displayErrorConnection() {
        msgPresenter.showMsgShort(getResources().getString(R.string.no_connection2), ContextCompat.getColor(getActivity().getApplicationContext(), R.color.alert));
    }

    @Override
    public void adapteList(List<Alert> list) {
        adaptador = new AdapteAlert(list);
        adaptador.setHasStableIds(true);
        adaptador.setOnItemClickListener(this);
        recycler.setAdapter(adaptador);
    }

    @Override
    public void gotoMain(Intent i) {

    }

    @Override
    public void showViewProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void showViewRefresh(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        swipeRefreshLayout.setVisibility(show ? View.VISIBLE : View.GONE);
        swipeRefreshLayout.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                swipeRefreshLayout.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    @Override
    public void displayLayoutConnection(final boolean connection) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
        layout_Connection.animate().setDuration(shortAnimTime).alpha(connection ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layout_Connection.setVisibility(connection ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void displayLayoutNocontent(final boolean content) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
        layout_noContent.animate().setDuration(shortAnimTime).alpha(content ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                layout_noContent.setVisibility(content ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onItemClick(RecyclerView.ViewHolder item, int position) {

    }

    @Override
    public boolean getAdded() {
        return isAdded();
    }

    @OnClick(R.id.text_try_again_content)
    public void error() {
        presenter.setView(this);
        presenter.getAlertList();
    }
}