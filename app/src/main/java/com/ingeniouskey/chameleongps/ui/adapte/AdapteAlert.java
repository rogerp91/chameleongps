package com.ingeniouskey.chameleongps.ui.adapte;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.domain.repository.datasource.api.HttpRestClient;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.imp.AlertRepositoryImp;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.AlertRepository;
import com.ingeniouskey.chameleongps.mvp.model.Alert;
import com.ingeniouskey.chameleongps.mvp.model.AlertParcel;
import com.ingeniouskey.chameleongps.ui.activity.MainActivity;
import com.ingeniouskey.chameleongps.ui.activity.MapsDetailActivity;
import com.ingeniouskey.chameleongps.ui.fragment.AlertFragment;
import com.ingeniouskey.chameleongps.utils.Constants;
import com.ingeniouskey.chameleongps.utils.Functions;
import com.ingeniouskey.chameleongps.utils.Prefs;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 23/12/2015.
 */
public class AdapteAlert extends RecyclerView.Adapter<AdapteAlert.ViewHolder> {

    protected static String TAG = AdapteAlert.class.getSimpleName();

    public List<Alert> list = null;

    public AdapteAlert(List<Alert> alist) {
        this.list = alist;
    }

    public interface OnItemClickListener {
        void onItemClick(RecyclerView.ViewHolder item, int position);
    }

    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_alert, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Alert aler = list.get(position);

        final String tipo = aler.getTipo();
        final int id = aler.getId();
        final String indentificador = aler.getIdentificador();
        final int pos_item = position;


        holder.id.setText(Integer.toString(aler.getId()));
        holder.indetificador.setText(aler.getIdentificador());
        holder.fecha_actual.setText(aler.getFecha_hora_enviado());
        final AppCompatButton btnLocationAppCompatButton = holder.btnLocation;

        holder.btnLocation.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UIApp.getContext, MapsDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.NAME_ACTIVITY, "Alert");
                intent.putExtra(Constants.ALERT, changeAlert(aler));

                UIApp.getContext.startActivity(intent);
                MainActivity.GET_ACTIVITY.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(MainActivity.GET_ACTIVITY).title(UIApp.getContext.getResources().getString(R.string.delete_alert))
                        .content(UIApp.getContext.getResources().getString(R.string.delete_alert_content) + ": " + tipo)
                        .positiveText(UIApp.getContext.getResources().getString(R.string.delete_alert_btn_confirm)).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(final MaterialDialog dialog, DialogAction which) {

                        final MaterialDialog.Builder builder = new MaterialDialog.Builder(MainActivity.GET_ACTIVITY).title("Eliminado: " + indentificador).content(R.string.log_out_content).progress(true, 0).progressIndeterminateStyle(true);
                        final MaterialDialog dialog1 = builder.build();
                        dialog1.show();

                        String token = Prefs.getString(Constants.TOKEN, "");

                        final AlertRepository repository = new AlertRepositoryImp();
                        OkHttpClient okHttpClient = new OkHttpClient();
                        Retrofit retrofit = new Retrofit.Builder().baseUrl(HttpRestClient.BASE_URL).client(okHttpClient).build();
                        HttpRestClient httpRestClient = retrofit.create(HttpRestClient.class);
                        Call<Void> voidCall = httpRestClient.performAlertDelete(HttpRestClient.PREFIX + token, id);

                        Log.d(TAG, "baseUrl:" + retrofit.baseUrl().url().url());

                        if (Functions.isOnline()) {
                            voidCall.enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Response<Void> response, Retrofit retrofit) {
                                    System.out.println("Adapte : " + response.code());
                                    switch (response.code()) {
                                        case 200:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 204:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            MainActivity.GET_ACTIVITY.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    repository.deleteAlert(id);
                                                    remove(pos_item);
                                                    dialog1.dismiss();
                                                }
                                            });
                                            break;
                                        case 400:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 401:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 403:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 404:
                                            dialog1.dismiss();
                                            Snackbar snackbar = Snackbar.make(MainActivity.VIEW_ACTIVITY, UIApp.getContext.getResources().getText(R.string.delete_alert_error_occurred), Snackbar.LENGTH_LONG);
                                            View snackBarView = snackbar.getView();
                                            snackBarView.setBackgroundColor(ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                            snackbar.show();
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                        case 500:
                                            Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                                            break;
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    dialog1.dismiss();
                                    System.out.println("Adapte : " + t.toString());
                                    Snackbar snackbar = Snackbar.make(MainActivity.VIEW_ACTIVITY, UIApp.getContext.getResources().getText(R.string.delete_alert_error_occurred), Snackbar.LENGTH_LONG);
                                    View snackBarView = snackbar.getView();
                                    snackBarView.setBackgroundColor(ContextCompat.getColor(UIApp.getContext, R.color.alert));
                                    snackbar.show();
                                }
                            });
                        } else {
                            Snackbar snackbar = Snackbar.make(MainActivity.VIEW_ACTIVITY, UIApp.getContext.getResources().getText(R.string.no_connection), Snackbar.LENGTH_LONG);
                            View snackBarView = snackbar.getView();
                            snackBarView.setBackgroundColor(ContextCompat.getColor(UIApp.getContext, R.color.alert));
                            snackbar.show();
                        }
                    }


                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                }).negativeText(UIApp.getContext.getResources().getString(R.string.delete_alert_btn_cancel)).show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    public void remove(int position) {
        list.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Campos respectivos de un item
        public TextView indetificador;
        public TextView id;
        public TextView fecha_actual;
        public AppCompatButton btnLocation;
        public AppCompatButton btnDelete;

        private AdapteAlert father = null;

        public ViewHolder(View view, AdapteAlert father) {
            super(view);
            view.setOnClickListener(this);
            this.father = father;
            indetificador = (TextView) view.findViewById(R.id.identificador);
            id = (TextView) view.findViewById(R.id.id);
            fecha_actual = (TextView) view.findViewById(R.id.fecha_actual);
            btnLocation = (AppCompatButton) view.findViewById(R.id.btnLocation);
            btnDelete = (AppCompatButton) view.findViewById(R.id.btnDelete);
        }

        @Override
        public void onClick(View v) {
            final OnItemClickListener listener = father.getOnItemClickListener();
            if (listener != null) {
                listener.onItemClick(this, getAdapterPosition());
            }
        }
    }

    //cambiar de un objeto realm a un objeto parcelable
    private AlertParcel changeAlert(final Alert alert) {
        AlertParcel alertParcel = new AlertParcel();
        alertParcel.setId(alert.getId());
        alertParcel.setTipo(alert.getTipo());
        alertParcel.setFecha_hora_enviado(alert.getFecha_hora_enviado());
        alertParcel.setLeido(alert.getLeido());
        alertParcel.setIdentificador(alert.getIdentificador());
        alertParcel.setLatitud(alert.getLatitud());
        alertParcel.setLongitud(alert.getLongitud());
        alertParcel.setVelocidad(alert.getVelocidad());
        alertParcel.setId_unidad(alert.getId_unidad());
        alertParcel.setId_perimetro(alert.getId_perimetro());
        alertParcel.setFecha_actual(alert.getFecha_actual());
        return alertParcel;
    }
}