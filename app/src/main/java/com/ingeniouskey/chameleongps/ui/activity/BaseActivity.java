package com.ingeniouskey.chameleongps.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.di.modules.ActivityModule;

import java.util.ArrayList;
import java.util.List;

import dagger.ObjectGraph;

/**
 * Created by Roger Patiño on 02/02/2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private ObjectGraph activityGraph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
    }

    protected abstract List<Object> getModules();

    private void injectDependencies() {
        UIApp IkApplication = (UIApp) getApplication();
        List<Object> activityScopeModules = (getModules() != null) ? getModules() : new ArrayList<>();
        activityScopeModules.add(new ActivityModule(this));
        activityGraph = IkApplication.buildGraphWithAditionalModules(activityScopeModules);
        inject(this);
    }

    @Override
    protected void onDestroy() {//destriur el modulos
        super.onDestroy();
        activityGraph = null;
    }

    public void inject(Object entityToGetInjected) {
        activityGraph.inject(entityToGetInjected);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void showDialogGooglePlay() {
        new MaterialDialog.Builder(this).title(getResources().getString(R.string.title_google_play)).positiveText(getResources().getString(R.string.google_play_positive)).negativeText(getResources().getString(R.string.google_play_negative))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gms&hl=es_419"));
                        startActivity(browserIntent);
                    }
                }).show();
    }
}