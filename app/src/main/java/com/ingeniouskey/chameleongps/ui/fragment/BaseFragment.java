package com.ingeniouskey.chameleongps.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.di.modules.FragmentModule;

import java.util.ArrayList;
import java.util.List;

import dagger.ObjectGraph;

/**
 * Created by Roger Patiño on 02/02/2016.
 */
public abstract class BaseFragment extends Fragment {

    private ObjectGraph activityGraph;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
    }

    //inyectar por actividades modulos de cada actividad
    protected abstract List<Object> getModules();

    //injectar dependencias
    private void injectDependencies() {
        UIApp IkApplication = (UIApp) getActivity().getApplication();
        List<Object> activityScopeModules = (getModules() != null) ? getModules() : new ArrayList<>();
        activityScopeModules.add(new FragmentModule(getActivity()));
        activityGraph = IkApplication.buildGraphWithAditionalModules(activityScopeModules);
        inject(this);
    }

    public void inject(Object entityToGetInjected) {
        activityGraph.inject(entityToGetInjected);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        activityGraph = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}