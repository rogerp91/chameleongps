package com.ingeniouskey.chameleongps.executor;

public interface MainThread {
	
	public void post(final Runnable runnable);
	
}
