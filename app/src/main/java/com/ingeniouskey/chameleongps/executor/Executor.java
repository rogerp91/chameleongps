package com.ingeniouskey.chameleongps.executor;

public interface Executor {
	
    public void run(final Interactor interactor);
    
}
