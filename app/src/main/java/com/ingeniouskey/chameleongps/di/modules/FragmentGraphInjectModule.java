package com.ingeniouskey.chameleongps.di.modules;

import com.ingeniouskey.chameleongps.ui.fragment.AlertFragment;
import com.ingeniouskey.chameleongps.ui.fragment.MapsFragment;
import com.ingeniouskey.chameleongps.ui.fragment.UnitFragment;

import dagger.Module;

/**
 * Created by Roger Patiño on 22/12/2015.
 */
@Module(injects = {
        MapsFragment.class,
        UnitFragment.class,
        AlertFragment.class}, complete = false)
public class FragmentGraphInjectModule {
}