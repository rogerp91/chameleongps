package com.ingeniouskey.chameleongps.di.modules;

import android.app.Activity;
import android.content.Context;

import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.Repository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 01/12/2015.
 */
@Module(
        includes = {
                ActivityGraphInjectModule.class,
                PresenterModule.class,
                InteractorModule.class,
                RestClientModule.class,
                RepositoryModule.class,
                SnackbarModule.class
        },
        library = true, complete = false
)
public class ActivityModule {

    private final Activity activityContext;

    public ActivityModule(Activity activityContext) {
        this.activityContext = activityContext;
    }

    @Provides
    public Context provideActivityContext() {
        return activityContext;
    }

    @Provides
    public Activity provideActivityActivity() {//para el fragment
        return activityContext;
    }
}
