package com.ingeniouskey.chameleongps.di.modules;

import com.ingeniouskey.chameleongps.mvp.presenter.imp.AlertPresenterImp;
import com.ingeniouskey.chameleongps.mvp.presenter.imp.LoginPresenterImp;
import com.ingeniouskey.chameleongps.mvp.presenter.imp.MapsPresenterImp;
import com.ingeniouskey.chameleongps.mvp.presenter.imp.UnitPresenterImp;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.AlertPresenter;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.LoginPresenter;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.MapsPresenter;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.UnitPresenter;
import com.ingeniouskey.chameleongps.ui.activity.LoginActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 01/12/2015.
 */
@Module(
        injects = {LoginActivity.class},
        library = true, complete = false
)
public class PresenterModule {

    @Provides
    @Singleton
    public LoginPresenter provideLoginPresenter(LoginPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    public MapsPresenter provideMapsPresenter(MapsPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    public AlertPresenter provideRegisterPresenter(AlertPresenterImp presenter) {
        return presenter;
    }

    @Provides
    @Singleton
    public UnitPresenter provideUnitPresenter(UnitPresenterImp presenter) {
        return presenter;
    }
}
