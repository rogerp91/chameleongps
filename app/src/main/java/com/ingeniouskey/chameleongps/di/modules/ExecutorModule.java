package com.ingeniouskey.chameleongps.di.modules;

import com.ingeniouskey.chameleongps.executor.Executor;
import com.ingeniouskey.chameleongps.executor.MainThread;
import com.ingeniouskey.chameleongps.executor.MainThreadImpl;
import com.ingeniouskey.chameleongps.executor.ThreadExecutor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 30/11/2015.
 */
@Module(library = true)
public class ExecutorModule {

    @Provides
    @Singleton
    public Executor provideExecutor(ThreadExecutor threadExecutor) {
        return threadExecutor;
    }

    @Provides
    @Singleton
    public MainThread provideMainThread(MainThreadImpl impl) {
        return impl;
    }

}
