package com.ingeniouskey.chameleongps.di.modules;

import com.ingeniouskey.chameleongps.components.ShowSnackbarMsg;
import com.ingeniouskey.chameleongps.mvp.view.GetView;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.ShowMsgPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(library = true)
public class SnackbarModule {

    private GetView iView;

    public SnackbarModule(GetView iView) {
        this.iView = iView;
    }

    @Provides
    @Singleton
    public GetView provideView() {
        return this.iView;
    }

    @Provides
    @Singleton
    public ShowMsgPresenter provideErrorManager() {
        return new ShowSnackbarMsg(iView);
    }
}
