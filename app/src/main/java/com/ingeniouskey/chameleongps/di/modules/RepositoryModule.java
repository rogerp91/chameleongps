package com.ingeniouskey.chameleongps.di.modules;

import com.ingeniouskey.chameleongps.domain.repository.datasource.db.imp.AlertRepositoryImp;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.imp.RepositoryImp;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.imp.UnitRepositoryImp;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.AlertRepository;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.Repository;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.UnitRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 05/02/2016.
 */
@Module(library = true, complete = false)
public class RepositoryModule {

    @Provides
    @Singleton
    UnitRepository provideUnitRepository(UnitRepositoryImp repository) {
        return repository;
    }

    @Provides
    @Singleton
    AlertRepository provideUnitRepository(AlertRepositoryImp repository) {
        return repository;
    }

    @Provides
    @Singleton
    Repository provideRepository() {
        return new RepositoryImp(new AlertRepositoryImp(), new UnitRepositoryImp());
    }
}