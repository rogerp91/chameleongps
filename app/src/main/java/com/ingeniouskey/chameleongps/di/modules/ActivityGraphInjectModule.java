package com.ingeniouskey.chameleongps.di.modules;

import com.ingeniouskey.chameleongps.ui.activity.LoginActivity;
import com.ingeniouskey.chameleongps.ui.activity.MainActivity;

import dagger.Module;

@Module(injects = {
        MainActivity.class,
        LoginActivity.class
},
        complete = false)
public class ActivityGraphInjectModule {
}
