package com.ingeniouskey.chameleongps.di;

import android.app.AlarmManager;
import android.content.Context;
import android.net.ConnectivityManager;

import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.di.modules.ExecutorModule;
import com.ingeniouskey.chameleongps.di.modules.RestClientModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 30/11/2015.
 */
@Module(
        injects = UIApp.class,
        library = true,
        includes = {
                ExecutorModule.class,
                RestClientModule.class
        }
)
public class AppModules {

    public UIApp app;

    public AppModules(UIApp app) {
        this.app = app;
    }

    @Provides
    public UIApp provideApplication() {
        return this.app;
    }

    @Provides
    @Singleton
    public ConnectivityManager provideConnectivityManager() {
        return (ConnectivityManager) this.app.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Provides
    @Singleton
    public AlarmManager provideAlarmManager() {
        return (AlarmManager) this.app.getSystemService(Context.ALARM_SERVICE);
    }

}
