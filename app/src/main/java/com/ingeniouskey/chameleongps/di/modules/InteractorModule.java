package com.ingeniouskey.chameleongps.di.modules;

import com.ingeniouskey.chameleongps.domain.interactor.imp.AlertInteractorImp;
import com.ingeniouskey.chameleongps.domain.interactor.imp.LoginInteractorImp;
import com.ingeniouskey.chameleongps.domain.interactor.imp.MapsInteractorImp;
import com.ingeniouskey.chameleongps.domain.interactor.imp.UnitInteractorImp;
import com.ingeniouskey.chameleongps.domain.interactor.interfaces.AlertInteractor;
import com.ingeniouskey.chameleongps.domain.interactor.interfaces.LoginInteractor;
import com.ingeniouskey.chameleongps.domain.interactor.interfaces.MapsInteractor;
import com.ingeniouskey.chameleongps.domain.interactor.interfaces.UnitInteractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Roger Patiño on 01/12/2015.
 */
@Module(library = true, complete = false)
public class InteractorModule {

    @Provides
    @Singleton
    public LoginInteractor provideLoginInteractor(LoginInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    public MapsInteractor provideIAlertInteractor(MapsInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    public AlertInteractor provideIAlertInteractor(AlertInteractorImp interactor) {
        return interactor;
    }

    @Provides
    @Singleton
    public UnitInteractor provideUnitInteractor(UnitInteractorImp interactor) {
        return interactor;
    }

}