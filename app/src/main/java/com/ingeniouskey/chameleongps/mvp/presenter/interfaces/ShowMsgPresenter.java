package com.ingeniouskey.chameleongps.mvp.presenter.interfaces;

public interface ShowMsgPresenter {

    void showMsgShort(String error, int color);

    void showMsgLong(String error, int color);

}