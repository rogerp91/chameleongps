package com.ingeniouskey.chameleongps.mvp.model;

/**
 * Created by Roger Patiño on 11/02/2016.
 */
public class Command {

    private int unit;
    private String message;

    public Command(int unit, String message) {
        this.unit = unit;
        this.message = message;
    }

}