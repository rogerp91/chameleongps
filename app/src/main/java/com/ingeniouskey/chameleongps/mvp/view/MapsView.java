package com.ingeniouskey.chameleongps.mvp.view;

import android.app.Activity;

import com.ingeniouskey.chameleongps.mvp.model.Unit;

import java.util.List;

/**
 * Created by Roger Patiño on 28/12/2015.
 */
public interface MapsView extends GetView {

    void showMsg(String mgs, int color);

    void displayError();

    void displayErrorConnection();

    Activity getActivity();

    void showViewProgress(final boolean show);

    void showViewRefresh(final boolean show);

    void setRefreshing(boolean refreshing);

    void displayLayoutConnection(final boolean connection);

    void displayLayoutNocontent(final boolean content);

    void showMapsAndMarker(boolean show);

    void displayApdateMaps(List<Unit> units);

    boolean getAdded();

}
