package com.ingeniouskey.chameleongps.mvp.model;

import android.widget.TextView;

import com.ingeniouskey.chameleongps.components.ProgressWheel;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Roger Patiño on 22/12/2015.
 */
public class ProfileItem {

    private TextView username;
    private TextView email;
    private CircleImageView circleImageView;
    private ProgressWheel progressWheel;

    public TextView getEmail() {
        return email;
    }

    public void setEmail(TextView email) {
        this.email = email;
    }

    public TextView getUsername() {
        return username;
    }

    public void setUsername(TextView username) {
        this.username = username;
    }

    public ProgressWheel getProgressWheel() {
        return progressWheel;
    }

    public void setProgressWheel(ProgressWheel progressWheel) {
        this.progressWheel = progressWheel;
    }

    public CircleImageView getCircleImageView() {
        return circleImageView;
    }

    public void setCircleImageView(CircleImageView circleImageView) {
        this.circleImageView = circleImageView;
    }

}
