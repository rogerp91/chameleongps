package com.ingeniouskey.chameleongps.mvp.presenter.interfaces;

import com.ingeniouskey.chameleongps.mvp.view.UnitView;

/**
 * Created by Roger Patiño on 10/02/2016.
 */
public interface UnitPresenter {

    void getUnitList();

    void getUnitListRefresh();

    void setView(UnitView view);

}