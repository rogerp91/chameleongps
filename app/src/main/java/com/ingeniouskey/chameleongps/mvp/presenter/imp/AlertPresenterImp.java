package com.ingeniouskey.chameleongps.mvp.presenter.imp;

import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.domain.interactor.interfaces.AlertInteractor;
import com.ingeniouskey.chameleongps.mvp.model.Alert;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.AlertPresenter;
import com.ingeniouskey.chameleongps.mvp.view.AlertView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Roger Patiño on 22/12/2015.
 */
public class AlertPresenterImp implements AlertPresenter {

    protected static String TAG = AlertPresenterImp.class.getSimpleName();
    AlertView view;
    AlertInteractor interactor;

    @Inject
    public AlertPresenterImp(AlertInteractor inInteractor) {
        this.interactor = inInteractor;
    }

    @Override
    public void setView(AlertView view) {
        if (view == null) {
            Log.e(TAG, "View must not be null!");
            throw new IllegalArgumentException("View must not be null!");
        }
        this.view = view;
    }

    @Override
    public void getAlertList() {//lista notrmal
        view.displayLayoutConnection(false);
        view.displayLayoutNocontent(false);
        view.showViewProgress(true);//cargando
        view.showViewRefresh(false);//refesh
        getList();
    }

    @Override
    public void getAlertListRefresh() {//lista con refresh
        view.showViewRefresh(true);//refesh
        view.setRefreshing(true);
        getList2();
    }

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    private void getList() {
        interactor.execute(true, new AlertInteractor.Callback() {
            @Override
            public void onSuccess(List<Alert> listAlert) {
                if (view.getAdded() && view.getActivity() != null) {
                    view.adapteList(listAlert);
                    view.showViewProgress(false);
                    view.showViewRefresh(true);
                }
            }

            @Override
            public void onErrorConnection() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorContect() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.error_occurred), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorConnectionVoid() {
                if (view.getAdded() && view.getActivity() != null) {
                    view.showViewProgress(false);
                    view.showViewRefresh(true);
                    view.displayLayoutConnection(true);
                }
            }

            @Override
            public void onErrorContectVoid() {
                if (view.getAdded() && view.getActivity() != null) {
                    view.showViewProgress(false);
                    view.showViewRefresh(true);
                    view.displayLayoutNocontent(true);
                }
            }

            @Override
            public void onError() {
                if (view.getAdded() && view.getActivity() != null) {
                    view.showViewProgress(false);
                    view.showViewRefresh(false);
                    view.displayError();
                }
            }

            @Override
            public void onError(String msgError) {
                if (view.getAdded() && view.getActivity() != null) {
                    view.showViewProgress(false);
                    view.showViewRefresh(false);
                    view.showMsg(msgError, ContextCompat.getColor(UIApp.getContext, R.color.alert));
                }
            }
        });
    }

    private void getList2() {
        interactor.execute(false, new AlertInteractor.Callback() {
            @Override
            public void onSuccess(List<Alert> listAlert) {
                if (view.getAdded() && view.getActivity() != null) {
                    view.adapteList(listAlert);
                    view.showViewRefresh(true);
                    view.setRefreshing(false);
                }
            }

            @Override
            public void onErrorConnection() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorContect() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.error_occurred), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorConnectionVoid() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorContectVoid() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.error_occurred), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onError() {
                if (view.getAdded() && view.getActivity() != null) {
                    view.showViewRefresh(false);
                    view.setRefreshing(false);
                    view.displayError();
                }
            }

            @Override
            public void onError(String msgError) {
                if (view.getAdded() && view.getActivity() != null) {
                    view.showViewRefresh(false);
                    view.setRefreshing(false);
                    view.showMsg(msgError, ContextCompat.getColor(UIApp.getContext, R.color.alert));
                }
            }
        });
    }
}