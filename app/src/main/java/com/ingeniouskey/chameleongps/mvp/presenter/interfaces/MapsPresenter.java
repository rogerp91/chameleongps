package com.ingeniouskey.chameleongps.mvp.presenter.interfaces;

import com.ingeniouskey.chameleongps.mvp.view.MapsView;

/**
 * Created by Roger Patiño on 28/12/2015.
 */
public interface MapsPresenter {

    void setView(MapsView view);

    void getAlertUnit();

    void getAlertUnitRefresh();

}