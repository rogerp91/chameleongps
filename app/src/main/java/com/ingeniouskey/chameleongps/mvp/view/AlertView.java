package com.ingeniouskey.chameleongps.mvp.view;

import android.app.Activity;
import android.content.Intent;

import com.ingeniouskey.chameleongps.mvp.model.Alert;

import java.util.List;

/**
 * Created by Roger Patiño on 22/12/2015.
 */
public interface AlertView extends GetView {

    void showView(final boolean show);

    void showMsg(String mgs, int color);

    void displayError();

    void displayErrorConnection();

    void adapteList(List<Alert> list);

    void gotoMain(Intent i);

    Activity getActivity();

    void showViewProgress(final boolean show);

    void showViewRefresh(final boolean show);

    void setRefreshing(boolean refreshing);

    void displayLayoutConnection(final boolean connection);

    void displayLayoutNocontent(final boolean content);

    boolean getAdded();

}