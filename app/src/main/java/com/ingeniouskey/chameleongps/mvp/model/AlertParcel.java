package com.ingeniouskey.chameleongps.mvp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Roger Patiño on 04/01/2016.
 */
public class AlertParcel implements Parcelable {

    private int id;
    private String tipo;
    private String fecha_hora_enviado;
    private String leido;
    private String identificador;
    private double latitud;
    private double longitud;
    private double velocidad;
    private int id_unidad;
    private int id_perimetro;
    private String fecha_actual;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFecha_hora_enviado() {
        return fecha_hora_enviado;
    }

    public void setFecha_hora_enviado(String fecha_hora_enviado) {
        this.fecha_hora_enviado = fecha_hora_enviado;
    }

    public String getLeido() {
        return leido;
    }

    public void setLeido(String leido) {
        this.leido = leido;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public int getId_unidad() {
        return id_unidad;
    }

    public void setId_unidad(int id_unidad) {
        this.id_unidad = id_unidad;
    }

    public int getId_perimetro() {
        return id_perimetro;
    }

    public void setId_perimetro(int id_perimetro) {
        this.id_perimetro = id_perimetro;
    }

    public String getFecha_actual() {
        return fecha_actual;
    }

    public void setFecha_actual(String fecha_actual) {
        this.fecha_actual = fecha_actual;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.tipo);
        dest.writeString(this.fecha_hora_enviado);
        dest.writeString(this.leido);
        dest.writeString(this.identificador);
        dest.writeDouble(this.latitud);
        dest.writeDouble(this.longitud);
        dest.writeDouble(this.velocidad);
        dest.writeInt(this.id_unidad);
        dest.writeInt(this.id_perimetro);
        dest.writeString(this.fecha_actual);
    }

    public AlertParcel() {
    }

    private AlertParcel(Parcel in) {
        this.id = in.readInt();
        this.tipo = in.readString();
        this.fecha_hora_enviado = in.readString();
        this.leido = in.readString();
        this.identificador = in.readString();
        this.latitud = in.readDouble();
        this.longitud = in.readDouble();
        this.velocidad = in.readDouble();
        this.id_unidad = in.readInt();
        this.id_perimetro = in.readInt();
        this.fecha_actual = in.readString();
    }

    public static final Creator<AlertParcel> CREATOR = new Creator<AlertParcel>() {
        public AlertParcel createFromParcel(Parcel source) {
            return new AlertParcel(source);
        }

        public AlertParcel[] newArray(int size) {
            return new AlertParcel[size];
        }
    };
}