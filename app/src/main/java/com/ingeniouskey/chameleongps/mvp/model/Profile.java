package com.ingeniouskey.chameleongps.mvp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 03/02/2016.
 */
public class Profile {

    @SerializedName("id")
    private int id;

    @SerializedName("usuario")
    private String usuario;

    @SerializedName("numero_identificacion")
    private String numero_identificacion;

    @SerializedName("nombre")
    private String nombre;

    @SerializedName("apellido")
    private String apellido;

    @SerializedName("nivel")
    private String nivel;

    @SerializedName("telefono")
    private String telefono;

    @SerializedName("terminos")
    private boolean terminos;

    @SerializedName("estado")
    private boolean estado;

    @SerializedName("idioma")
    private String idioma;

    @SerializedName("titulo")
    private String titulo;

    @SerializedName("formato_fecha")
    private String formato_fecha;

    @SerializedName("formato_hora")
    private String formato_hora;

    @SerializedName("telefono_casa")
    private String telefono_casa;

    @SerializedName("telefono_movil")
    private String telefono_movil;

    @SerializedName("telefono_oficina")
    private String telefono_oficina;

    @SerializedName("direccion_calle")
    private String direccion_calle;

    @SerializedName("direccion_2")
    private String direccion_2;

    @SerializedName("direccion_estado")
    private String direccion_estado;

    @SerializedName("direccion_ciudad")
    private String direccion_ciudad;

    @SerializedName("direccion_codigozip")
    private String direccion_codigozip;

    @SerializedName("direccion_pais")
    private String direccion_pais;

    @SerializedName("correo_electronico")
    private String correo_electronico;

    @SerializedName("unidad_velocidad")
    private String unidad_velocidad;

    @SerializedName("zona_horaria")
    private String zona_horaria;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNumero_identificacion() {
        return numero_identificacion;
    }

    public void setNumero_identificacion(String numero_identificacion) {
        this.numero_identificacion = numero_identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public boolean isTerminos() {
        return terminos;
    }

    public void setTerminos(boolean terminos) {
        this.terminos = terminos;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFormato_fecha() {
        return formato_fecha;
    }

    public void setFormato_fecha(String formato_fecha) {
        this.formato_fecha = formato_fecha;
    }

    public String getFormato_hora() {
        return formato_hora;
    }

    public void setFormato_hora(String formato_hora) {
        this.formato_hora = formato_hora;
    }

    public String getTelefono_casa() {
        return telefono_casa;
    }

    public void setTelefono_casa(String telefono_casa) {
        this.telefono_casa = telefono_casa;
    }

    public String getTelefono_movil() {
        return telefono_movil;
    }

    public void setTelefono_movil(String telefono_movil) {
        this.telefono_movil = telefono_movil;
    }

    public String getTelefono_oficina() {
        return telefono_oficina;
    }

    public void setTelefono_oficina(String telefono_oficina) {
        this.telefono_oficina = telefono_oficina;
    }

    public String getDireccion_calle() {
        return direccion_calle;
    }

    public void setDireccion_calle(String direccion_calle) {
        this.direccion_calle = direccion_calle;
    }

    public String getDireccion_2() {
        return direccion_2;
    }

    public void setDireccion_2(String direccion_2) {
        this.direccion_2 = direccion_2;
    }

    public String getDireccion_estado() {
        return direccion_estado;
    }

    public void setDireccion_estado(String direccion_estado) {
        this.direccion_estado = direccion_estado;
    }

    public String getDireccion_ciudad() {
        return direccion_ciudad;
    }

    public void setDireccion_ciudad(String direccion_ciudad) {
        this.direccion_ciudad = direccion_ciudad;
    }

    public String getDireccion_codigozip() {
        return direccion_codigozip;
    }

    public void setDireccion_codigozip(String direccion_codigozip) {
        this.direccion_codigozip = direccion_codigozip;
    }

    public String getDireccion_pais() {
        return direccion_pais;
    }

    public void setDireccion_pais(String direccion_pais) {
        this.direccion_pais = direccion_pais;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public String getUnidad_velocidad() {
        return unidad_velocidad;
    }

    public void setUnidad_velocidad(String unidad_velocidad) {
        this.unidad_velocidad = unidad_velocidad;
    }

    public String getZona_horaria() {
        return zona_horaria;
    }

    public void setZona_horaria(String zona_horaria) {
        this.zona_horaria = zona_horaria;
    }
}