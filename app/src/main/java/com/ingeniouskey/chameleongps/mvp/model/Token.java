package com.ingeniouskey.chameleongps.mvp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roger Patiño on 02/02/2016.
 */
public class Token {

    @SerializedName("token")
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
