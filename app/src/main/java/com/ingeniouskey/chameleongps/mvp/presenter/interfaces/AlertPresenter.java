package com.ingeniouskey.chameleongps.mvp.presenter.interfaces;

import com.ingeniouskey.chameleongps.mvp.view.AlertView;

/**
 * Created by Roger Patiño on 22/12/2015.
 */
public interface AlertPresenter {

    void getAlertList();
    void getAlertListRefresh();
    void setView(AlertView view);

}
