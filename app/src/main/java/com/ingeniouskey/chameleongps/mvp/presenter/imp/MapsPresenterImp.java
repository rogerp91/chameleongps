package com.ingeniouskey.chameleongps.mvp.presenter.imp;

import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.domain.interactor.interfaces.MapsInteractor;
import com.ingeniouskey.chameleongps.mvp.model.Unit;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.MapsPresenter;
import com.ingeniouskey.chameleongps.mvp.view.MapsView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Roger Patiño on 28/12/2015.
 */
public class MapsPresenterImp implements MapsPresenter {

    protected static String TAG = MapsPresenterImp.class.getSimpleName();
    MapsView view;

    MapsInteractor interactor;

    @Inject
    public MapsPresenterImp(MapsInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void setView(MapsView view) {
        if (view == null) {
            Log.e(TAG, "View must not be null!");
            throw new IllegalArgumentException("View must not be null!");
        }
        this.view = view;
    }

    @Override
    public void getAlertUnit() {
        //view.displayLayoutConnection(false);
        //view.displayLayoutNocontent(false);
        view.showViewProgress(true);//cargando
        view.showViewRefresh(false);//refesh
        getList(true);
    }

    @Override
    public void getAlertUnitRefresh() {
        view.showViewProgress(true);//cargando
        view.showViewRefresh(false);//refesh
        getList(false);
    }

    //----------------------------------------------------------------------------------------------
    private void getList(boolean type) {
        interactor.execute(type, new MapsInteractor.Callback() {
            @Override
            public void onSuccess(List<Unit> listUnit) {
                if (view.getAdded() && view.getActivity() != null) {
                    view.showViewProgress(false);
                    view.showViewRefresh(false);
                    view.displayApdateMaps(listUnit);
                    //view.showMapsAndMarker(true);
                }
            }

            @Override
            public void onErrorConnection() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.no_connection2), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorContect() {
                view.showMsg(UIApp.getContext.getResources().getString(R.string.error_occurred), ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorConnectionVoid() {
                Log.d(TAG, "onErrorConnectionVoid");
                if (view.getAdded() && view.getActivity() != null) {
                    view.showMapsAndMarker(false);
                    view.showViewProgress(false);
                    view.showViewRefresh(true);
                    view.displayLayoutConnection(true);
                }
            }

            @Override
            public void onErrorContectVoid() {
                Log.d(TAG, "onErrorContectVoid");
                if (view.getAdded() && view.getActivity() != null) {
                    view.showMapsAndMarker(false);
                    view.showViewProgress(false);
                    view.showViewRefresh(true);
                    view.displayLayoutNocontent(true);
                }
            }

            @Override
            public void onError() {
                Log.d(TAG, "onError");
                if (view.getAdded() && view.getActivity() != null) {
                    view.showViewProgress(false);
                    view.showViewRefresh(false);
                    view.displayError();
                }
            }
        });
    }
}