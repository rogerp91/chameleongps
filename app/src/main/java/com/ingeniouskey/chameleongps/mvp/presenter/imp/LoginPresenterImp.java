package com.ingeniouskey.chameleongps.mvp.presenter.imp;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.domain.interactor.interfaces.LoginInteractor;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.LoginPresenter;
import com.ingeniouskey.chameleongps.mvp.view.LoginView;
import com.ingeniouskey.chameleongps.ui.activity.MainActivity;
import com.ingeniouskey.chameleongps.utils.Constants;
import com.ingeniouskey.chameleongps.utils.Functions;
import com.ingeniouskey.chameleongps.utils.Prefs;

import javax.inject.Inject;

/**
 * Created by Roger Patiño on 15/12/2015.
 */
public class LoginPresenterImp implements LoginPresenter {

    //TAG
    protected static String TAG = LoginPresenterImp.class.getSimpleName();

    //inject
    LoginView view;
    LoginInteractor interactor;

    @Inject
    public LoginPresenterImp(LoginInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void setView(LoginView view) {
        if (view == null) {
            Log.e(TAG, "View must not be null!");
            throw new IllegalArgumentException("View must not be null!");
        }
        this.view = view;
    }

    @Override
    public void validateCredentials(String user, String password) {
        //verificar que no hay error
        boolean cancel = false;

        //validate datos
        if (TextUtils.isEmpty(password)) {
            cancel = true;
            this.view.displayInfoPassword();
        } else {
            if (!(password.length() > 3)) {
                cancel = true;
                view.displayInfoPasswordSize();
            }
        }

        if (TextUtils.isEmpty(user)) {
            cancel = true;
            this.view.displayInfoUser();
        } else {
            if (!(user.length() > 3)) {
                cancel = true;
                this.view.displayInfoUserSize();
            }
        }
        if (!cancel) {
            if (Functions.isOnline()) {
                view.showView(true);//quitar el progress
                postLogin(user, password);
            } else {
                view.showView(false);//quitar el progress
                view.displayErrorConnection();
            }
        }
    }

    private void postLogin(String user, String password) {
        interactor.execute(user, password, new LoginInteractor.Callback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess");
                Prefs.putBoolean(Constants.SESSION, true);
                Prefs.putBoolean(Constants.LOGIN, true);
                view.gotoMain(new Intent(UIApp.getContext, MainActivity.class));
            }

            @Override
            public void onErrorData() {
                view.showView(false);//colocar el progress de cargando
                view.displayErrorData();
            }

            @Override
            public void onError(String msgError) {
                view.showView(false);//colocar el progress de cargando
                view.displayMsg(msgError, ContextCompat.getColor(UIApp.getContext, R.color.alert));
            }

            @Override
            public void onErrorProfile() {
                view.displayInfoProfile();
            }
        });
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }
}