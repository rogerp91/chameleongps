package com.ingeniouskey.chameleongps.mvp.model;

/**
 * Created by Roger Patiño on 17/12/2015.
 */
public class Login {

    private String usuario;
    private String clave;
    private String device;

    public Login(String usuario, String clave, String device) {
        this.usuario = usuario;
        this.clave = clave;
        this.device = device;
    }
}
