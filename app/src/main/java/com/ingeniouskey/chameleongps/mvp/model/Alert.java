package com.ingeniouskey.chameleongps.mvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Roger Patiño on 23/12/2015.
 */
public class Alert extends RealmObject {

    @PrimaryKey
    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("tipo")
    private String tipo;

    @Expose
    @SerializedName("fecha_hora_enviado")
    private String fecha_hora_enviado;

    @Expose
    @SerializedName("leido")
    private String leido;

    @Expose
    @SerializedName("identificador")
    private String identificador;

    @Expose
    @SerializedName("latitud")
    private double latitud;

    @Expose
    @SerializedName("longitud")
    private double longitud;

    @Expose
    @SerializedName("velocidad")
    private double velocidad;

    @Expose
    @SerializedName("id_unidad")
    private int id_unidad;

    @Expose
    @SerializedName("id_perimetro")
    private int id_perimetro;

    @Expose
    @SerializedName("id_usuario")
    private int id_usuario;

    @Expose
    @SerializedName("fecha_actual")
    private String fecha_actual;

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getLeido() {
        return leido;
    }

    public void setLeido(String leido) {
        this.leido = leido;
    }

    public String getFecha_hora_enviado() {
        return fecha_hora_enviado;
    }

    public void setFecha_hora_enviado(String fecha_hora_enviado) {
        this.fecha_hora_enviado = fecha_hora_enviado;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public int getId_unidad() {
        return id_unidad;
    }

    public void setId_unidad(int id_unidad) {
        this.id_unidad = id_unidad;
    }

    public int getId_perimetro() {
        return id_perimetro;
    }

    public void setId_perimetro(int id_perimetro) {
        this.id_perimetro = id_perimetro;
    }

    public String getFecha_actual() {
        return fecha_actual;
    }

    public void setFecha_actual(String fecha_actual) {
        this.fecha_actual = fecha_actual;
    }
}