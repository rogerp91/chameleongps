package com.ingeniouskey.chameleongps.mvp.view;

import android.view.View;

public interface GetView {

    public abstract View getView();

}
