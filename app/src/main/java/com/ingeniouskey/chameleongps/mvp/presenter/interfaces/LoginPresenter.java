package com.ingeniouskey.chameleongps.mvp.presenter.interfaces;

import com.ingeniouskey.chameleongps.mvp.view.LoginView;

/**
 * Created by Roger Patiño on 15/12/2015.
 */
public interface LoginPresenter {

    void setView(LoginView view);

    void validateCredentials(String user, String password);

    void onDestroy();

    void onPause();

    void onResume();

}
