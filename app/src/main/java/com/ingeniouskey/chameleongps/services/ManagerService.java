package com.ingeniouskey.chameleongps.services;

/**
 * Created by Roger Patiño on 02/12/2015.
 */
public class ManagerService {

    public static final String ACTION_START_PROFILE = "START_PROFILE"; // Action to start
    public static final String ACTION_NOT_CONNECTION = "NOT_CONNECTION"; // Action to start
    public static final String ACTION_SUCCESS_PROFILE = "SUCCESS_PROFILE"; // Action to start
    public static final String ACTION_ERROR_PROFILE = "ERROR_PROFILE"; // Action to start
    public static final String ACTION_LOG_OUT = "LOG_OUT"; // Action to start


    public static final String ACTION_START = "START"; // Action to start
    public static final String ACTION_STOP = "STOP"; // Action to stop
    public static final String ACTION_KEEPALIVE = "KEEPALIVE"; // Action to keep alive used by alarm manager
    public static final String ACTION_RECONNECT = "RECONNECT"; // Action to reconnect
    public static final String ACTION_CANCEL_RECONNECT = "RECONNECT"; // Action to reconnect
    public static final String ACTION_ALIVE = "ALIVE"; // Action to reconnect
    public static final String ACTION_STARTED = "STARTED"; // Action to reconnect
    public static final String ACTION_SCHEDULE = "SCHEDULE"; // Action to reconnect

    //intrucciones
    public static final String ACTION_INIT_APP = "INIT_APP"; // Action to reconnect
    public static final String ACTION_CLOSE_APP = "CLOSE_APP";
    public static final String ACTION_NAME_ACTIVITY = "NAME_ACTIVITY";

    public static final String CHECK_PROFILE = "CHECK_PROFILE";
    public static final String CALCULATE_PROFILE = "CALCULATE_PROFILE";


}
