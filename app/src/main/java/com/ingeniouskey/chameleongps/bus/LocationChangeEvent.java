package com.ingeniouskey.chameleongps.bus;

import com.ingeniouskey.chameleongps.mvp.model.Alert;

/**
 * Created by Roger Patiño on 29/12/2015.
 */
public class LocationChangeEvent {

    public Alert alert;

    public LocationChangeEvent(Alert alert) {
        this.alert = alert;
    }

    public Alert getAlert() {
        return alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }

}
