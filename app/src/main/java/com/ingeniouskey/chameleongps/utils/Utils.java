package com.ingeniouskey.chameleongps.utils;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.components.BadgeDrawable;

/**
 * Created by Roger Patiño on 10/02/2016.
 */
public class Utils {

    public static void setBadgeCount(LayerDrawable icon, int count) {

        BadgeDrawable badge;
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(UIApp.getContext);
        }
        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

}