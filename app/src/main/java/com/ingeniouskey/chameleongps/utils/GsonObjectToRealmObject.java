package com.ingeniouskey.chameleongps.utils;

import com.ingeniouskey.chameleongps.domain.object.realm.UnitObject;
import com.ingeniouskey.chameleongps.mvp.model.Unit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roger Patiño on 05/02/2016.
 */
public class GsonObjectToRealmObject {

    public static List<UnitObject> UnitObjectRealm(List<Unit> unitList) {
        List<UnitObject> unitObjects = new ArrayList<>();
        for (Unit unit : unitList) {
            UnitObject object = new UnitObject();
            object.setId(unit.getId());
            object.setLatitud(unit.getLatitud());
            object.setLongitud(unit.getLongitud());
            object.setIdentificador(unit.getIdentificador());
            object.setDisponibilidad(unit.isDisponibilidad());
            object.setEncendido(unit.isEncendido());
            object.setVelocidad(unit.getVelocidad());
            object.setId_tipo_unidad(unit.getId_tipo_unidad());
            object.setTipo(unit.getTipo());
            object.setNombre(unit.getNombre());
            object.setActualizacion(unit.getActualizacion());
            object.setEn_servicio(unit.isEn_servicio());
            object.setId_usuario(unit.getId_usuario());
            object.setFecha_actual(unit.getFecha_actual());
            unitObjects.add(object);
        }
        return unitObjects;
    }

    public static List<Unit> UnitObject(List<UnitObject> unitList) {
        List<Unit> unitObjects = new ArrayList<>();
        for (UnitObject unit : unitList) {
            Unit object = new Unit();
            object.setId(unit.getId());
            object.setLatitud(unit.getLatitud());
            object.setLongitud(unit.getLongitud());
            object.setIdentificador(unit.getIdentificador());
            object.setDisponibilidad(unit.isDisponibilidad());
            object.setEncendido(unit.isEncendido());
            object.setVelocidad(unit.getVelocidad());
            object.setId_tipo_unidad(unit.getId_tipo_unidad());
            object.setTipo(unit.getTipo());
            object.setNombre(unit.getNombre());
            object.setActualizacion(unit.getActualizacion());
            object.setEn_servicio(unit.isEn_servicio());
            object.setId_usuario(unit.getId_usuario());
            object.setFecha_actual(unit.getFecha_actual());
            unitObjects.add(object);
        }
        return unitObjects;
    }

}