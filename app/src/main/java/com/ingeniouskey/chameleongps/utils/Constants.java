package com.ingeniouskey.chameleongps.utils;

public class Constants {

    // claves compartida de seseion y introducion
    public static final String LOGIN = "login";
    public static final String INTRODUTION = "introdution";

    //session
    public static final String SESSION = "session";

    //token
    public static final String TOKEN = "token";
    public static final String REFRESH = "refresh";

    // account
    public static final String ADD_ACCOUNT_TYPE = "chameleongps.com";
    public static final String AUTH_TOKEN_TYPE = "AuthTokenType";
    public static final String ACCOUNT_TYPE = "chameleongps.com";
    public static final String ACCOUNT_NAME = "ChameleonGPS";
    public static final String AUTHTOKEN_TYPE_READ_ONLY = "Read only";
    public static final String AUTHTOKEN_TYPE_READ_ONLY_LABEL = "Read only access to an ChameleonGPS account";
    public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";
    public static final String AUTHTOKEN_TYPE_FULL_ACCESS_LABEL = "Full access to an ChameleonGPS account";

    //HTTP
    public static final String CODE_HTTP = "code";
    public static final int CODE_401 = 401;

    public static final String ID = "id";

    //register

    public static final String PASAPORTE = "pasaporte";
    public static final String NOMBRE = "nombre";
    public static final String APELLIDO = "apellido";
    public static final String CORREO = "correo";
    public static final String IDIOMA = "idioma";
    public static final String ZONA = "zona";
    public static final String UNIDAD = "unidad";
    public static final String FORMATO_FECHA = "formato_fecha";
    public static final String FORMATO_HORA = "formato_hora";
    public static final String TELEFONO_CONTACTO = "telefono_contacto";
    public static final String TELEFONO_CASA = "telefono_casa";
    public static final String TELEFONO_MOVIL = "telefono_movil";
    public static final String TELEFONO_OFICINA = "telefono_oficina";
    public static final String DIRECCION_CALLE = "diraccion_calle";
    public static final String DIRECCION = "direccion";
    public static final String CODIGO_POSTAL = "codigo_postal";
    public static final String CIUDAD = "ciudad";
    public static final String ESTADO = "estado";
    public static final String PAIS = "pais";
    public static final String USUARIO = "usuario";
    public static final String CONTRASENA = "contrasena";
    public static final String CONFIRMAR = "confirmar";
    public static final String ROLE = "role";

    //MainActivity
    public static final String SUCESS_PROFILE = "SUCESS_PROFILLE";
    public static final String NAME_PROFILE = "NAME_PROFILLE";
    public static final String EMAIL_PROFILE = "EMAIL_PROFILLE";
    public static final String ERROR_PROFILE = "ERROR_PROFILLE";
    public static final String IMAGEN_PROFILE = "IMAGEN_PROFILLE";
    public static final String PROGRESS_PROFILE = "NAME_PROFILLE";


    //alert apadat6e
    public static final String ALERT = "ALERT";
    public static final String NAME_ACTIVITY = "NAME_ACTIVITY";
}// endClass	
