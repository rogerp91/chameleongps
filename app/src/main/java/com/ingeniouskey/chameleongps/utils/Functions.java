package com.ingeniouskey.chameleongps.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Patterns;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.ingeniouskey.chameleongps.UIApp;

public class Functions {//funciones de ayuda

    protected static String TAG = Functions.class.getSimpleName();

    public static boolean isTextSize(String text) {
        return text.length() > 3;
    }

    public static boolean isText(String text) {
        return text.matches("[a-zA-z]+([ '-][a-zA-Z]+)*");
    }

    public static boolean isEmailValid(String email) {
        return email.contains("@");
    }

    public static boolean isCedula(String cedula) {
        return cedula.length() > 5;
    }

    public static boolean isPhone(String phone) {
        return Patterns.PHONE.matcher(phone).matches();
    }

    public boolean validCellPhone(String number) {
        return Patterns.PHONE.matcher(number).matches();
    }

    // internet
    public static boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) UIApp.getContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static boolean checkPlayServices() {//verificar si existe google play services
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(UIApp.getContext);
        if (result != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    public static boolean isPermissionGranted(String permission) {
        if (ActivityCompat.checkSelfPermission(UIApp.getContext, permission) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UIApp.getContext, permission) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isLolipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static boolean isMarshmallow() {
        return Build.VERSION.SDK_INT > 22;
    }
}//endClass