package com.ingeniouskey.chameleongps;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;

import com.ingeniouskey.chameleongps.di.AppModules;
import com.ingeniouskey.chameleongps.utils.Prefs;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.util.List;

import dagger.ObjectGraph;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Roger Patiño on 02/02/2016.
 */
public class UIApp extends Application {

    protected static String TAG = UIApp.class.getSimpleName();

    public static volatile Context getContext;
    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();

        //conext global
        getContext = getApplicationContext();
        //pref
        new Prefs.Builder().setContext(this).setMode(ContextWrapper.MODE_PRIVATE).setPrefsName(getPackageName()).setUseDefaultSharedPreference(true).build();

        RealmConfiguration config = new RealmConfiguration.Builder(this).deleteRealmIfMigrationNeeded().build();//instacncia
        Realm.setDefaultConfiguration(config);//migracion por defector evitar error

        initDependencyInjection();
        initImageLoader();

    }

    //modulos adicionales
    public ObjectGraph buildGraphWithAditionalModules(List<Object> modules) {
        if (modules == null) {
            throw new IllegalArgumentException("You can't plus a null module, review your getModules() implementation");
        }
        return objectGraph.plus(modules.toArray());
    }

    //grafo de inyecciones
    public void initDependencyInjection() {
        objectGraph = ObjectGraph.create(new AppModules(this));
        objectGraph.inject(this);
        objectGraph.injectStatics();
    }


    public static void initImageLoader() {//control de cache para la imagenes
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(getContext);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app
        ImageLoader.getInstance().init(config.build());
    }
}