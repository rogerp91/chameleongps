package com.ingeniouskey.chameleongps.domain.repository.datasource.api;

import com.ingeniouskey.chameleongps.mvp.model.Alert;
import com.ingeniouskey.chameleongps.mvp.model.Command;
import com.ingeniouskey.chameleongps.mvp.model.Login;
import com.ingeniouskey.chameleongps.mvp.model.Profile;
import com.ingeniouskey.chameleongps.mvp.model.Token;
import com.ingeniouskey.chameleongps.mvp.model.Unit;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Roger Patiño on 06/01/2016.
 */
public interface HttpRestClient {

    public static String BASE_URL = "http://ng.chameleongps.com/api/";
    public static String PREFIX = "Bearer ";
    public static String VERSION = "v2/";

    //login
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST(VERSION + "auth/login")
    Call<Token> performLogin(@Body Login login);

    //perfil
    @GET(VERSION + "profile/me?device=mobile")
    Call<Profile> performProfile(@Header("Authorization") String authorization);

    //unidad
    @GET(VERSION + "unit?fields=unidad.id,unidad.latitud,unidad.longitud,unidad.identificador,unidad.disponibilidad,unidad.encendido,unidad.velocidad,unidad.id_tipo_unidad,tipo_unidad.tipo,unidad.nombre,unidad.actualizacion,unidad.en_servicio,unidad_usuario.id_usuario&sort=identificador")
    Call<List<Unit>> performUnit(@Header("Authorization") String authorization);

    //alertas
    @GET(VERSION + "alert?fields=alarma.id,alarma.tipo,alarma.fecha_hora_enviado,alarma.leido,unidad.identificador,posicion.latitud,posicion.longitud,posicion.velocidad,alarma.id_unidad,alarma.id_perimetro&limit=15&offset=0&sort=-fecha_hora_enviado")
    Call<List<Alert>> performAlert(@Header("Authorization") String authorization);

    //alert con su id
    @DELETE(VERSION + "alert/{id}")
    Call<Void> performAlertDelete(@Header("Authorization") String authorization, @Path("id") int id);

    @POST(VERSION + "commands")
    Call<Void> performAlertCommands(@Header("Authorization") String authorization, @Body Command command);
}