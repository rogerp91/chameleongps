package com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces;

import com.ingeniouskey.chameleongps.exception.ObtainException;
import com.ingeniouskey.chameleongps.exception.PersistException;
import com.ingeniouskey.chameleongps.exception.UniqueException;
import com.ingeniouskey.chameleongps.mvp.model.Alert;

import java.util.List;

/**
 * Created by Roger Patiño on 05/02/2016.
 */
public interface AlertRepository {

    List<Alert> obtainAlert();

    void persistAlert(List<Alert> alertList) throws UniqueException, PersistException;

    void deleteAlert(int id);

    void clear();
}