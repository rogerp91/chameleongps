package com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces;

/**
 * Created by Roger Patiño on 11/02/2016.
 */
public interface Repository {

    AlertRepository getAlert();

    UnitRepository getUnit();

}