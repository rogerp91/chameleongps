package com.ingeniouskey.chameleongps.domain.interactor.interfaces;

import com.ingeniouskey.chameleongps.mvp.model.Unit;

import java.util.List;

/**
 * Created by Roger Patiño on 28/12/2015.
 */
public interface MapsInteractor {

    interface Callback {

        void onSuccess(final List<Unit> listUnit);

        void onErrorConnection();

        void onErrorContect();

        void onErrorConnectionVoid();

        void onErrorContectVoid();

        void onError();
    }

    void execute(boolean typeMethod, Callback callBackAnt);

}
