package com.ingeniouskey.chameleongps.domain.interactor.imp;

import android.util.Log;

import com.ingeniouskey.chameleongps.domain.interactor.interfaces.AlertInteractor;
import com.ingeniouskey.chameleongps.domain.repository.datasource.api.HttpRestClient;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.AlertRepository;
import com.ingeniouskey.chameleongps.exception.PersistException;
import com.ingeniouskey.chameleongps.exception.UniqueException;
import com.ingeniouskey.chameleongps.executor.Executor;
import com.ingeniouskey.chameleongps.executor.Interactor;
import com.ingeniouskey.chameleongps.executor.MainThread;
import com.ingeniouskey.chameleongps.mvp.model.Alert;
import com.ingeniouskey.chameleongps.utils.Constants;
import com.ingeniouskey.chameleongps.utils.Functions;
import com.ingeniouskey.chameleongps.utils.Prefs;

import java.util.List;

import javax.inject.Inject;

import retrofit.Call;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 23/12/2015.
 */
public class AlertInteractorImp implements Interactor, AlertInteractor {

    protected static String TAG = AlertInteractorImp.class.getSimpleName();

    private final Executor executor;
    private final MainThread mainThread;
    private AlertInteractor.Callback callback;
    private boolean typeMethod;
    String token = "";

    AlertRepository repository;

    @Inject
    HttpRestClient restClient;

    @Inject
    public AlertInteractorImp(Executor executor, MainThread mainThread, AlertRepository repository) {
        this.executor = executor;
        this.mainThread = mainThread;
        this.repository = repository;
        token = Prefs.getString(Constants.TOKEN, "");
    }

    @Override
    public void execute(boolean typeMethod, AlertInteractor.Callback callBackAnt) {
        if (callBackAnt == null) {
            throw new IllegalArgumentException("Callback must not be null or response would not be able to be notified.");
        }

        this.typeMethod = typeMethod;
        this.callback = callBackAnt;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                List<Alert> alist = repository.obtainAlert();//obtener listado local
                boolean check = alist.size() == 0 || alist == null;//verificar si hay algo
                boolean internet = Functions.isOnline();//verificar si hay internet

                if (typeMethod) { //buscar listado por primera vez
                    if (check) { //si no hay nada
                        if (internet) {//si hay internet y no hay nada, buscar datos de servidor
                            getList();
                        } else {//si no hay nada y no hay intenet mostar informacion del layout completo
                            notifyPetitionErrorConnectVoid();
                        }
                    } else {//si hay algo almacenado en la base de dato mostrar
                        notifyPetitionSuccess(alist);
                    }
                } else {
                    if (!internet && !check) { //si no hay internet, pero hay dato en la base de dato, manda error
                        notifyPetitionErrorConnect();
                    } else {
                        if (!internet && check) {//
                            notifyPetitionErrorConnectVoid();
                        } else {
                            if (internet && check) {
                                getList();
                            } else {
                                if (internet && !check) {
                                    getList();
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    public void getList() {
        Call<List<Alert>> alertCall = restClient.performAlert(HttpRestClient.PREFIX + token);
        alertCall.enqueue(new retrofit.Callback<List<Alert>>() {
            @Override
            public void onResponse(Response<List<Alert>> response, Retrofit retrofit) {
                System.out.println("RESPONSE: " + response.body());
                boolean checkError = false;
                switch (response.code()) {
                    case 200:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        if (response.body() != null || !response.body().equals("")) {
                            List<Alert> alertList = response.body();
                            try {
                                repository.persistAlert(alertList);
                            } catch (UniqueException e) {
                                e.printStackTrace();
                                checkError = true;
                            } catch (PersistException e) {
                                e.printStackTrace();
                                checkError = true;
                            }

                            if (!checkError) {
                                notifyPetitionSuccess(alertList);
                            } else {
                                List<Alert> alist = null;//obtener listado local
                                alist = repository.obtainAlert();
                                boolean check = alist.size() == 0 || alist == null;//verificar si hay algo
                                if (check) {
                                    notifyPetitionErrorContentVoid();
                                } else {
                                    notifyPetitionError();
                                }
                            }
                        }
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 400:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 401:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 403:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 404:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 500:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(TAG, t.toString());
                List<Alert> alist = repository.obtainAlert();//obtener listado local
                boolean check = alist.size() == 0 || alist == null;//verificar si hay algo
                if (!check) { //si no hay internet, pero hay dato en la base de dato, manda error
                    notifyPetitionErrorConnect();
                } else {
                    notifyPetitionErrorConnectVoid();
                }
            }
        });
    }

    private void notifyPetitionSuccess(final List<Alert> listAlert) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess(listAlert);
            }
        });
    }

    private void notifyPetitionError() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError();
            }
        });
    }

    private void notifyPetitionErrorConnect() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConnection();
            }
        });
    }

    private void notifyPetitionErrorContent() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContect();
            }
        });
    }

    private void notifyPetitionErrorConnectVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorConnectionVoid();
            }
        });
    }

    private void notifyPetitionErrorContentVoid() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorContectVoid();
            }
        });
    }
}