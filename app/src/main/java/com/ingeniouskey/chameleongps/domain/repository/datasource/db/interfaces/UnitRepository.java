package com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces;

import com.ingeniouskey.chameleongps.domain.object.realm.UnitObject;
import com.ingeniouskey.chameleongps.exception.ObtainException;
import com.ingeniouskey.chameleongps.exception.PersistException;
import com.ingeniouskey.chameleongps.exception.UniqueException;
import com.ingeniouskey.chameleongps.mvp.model.Unit;

import java.util.List;

/**
 * Created by Roger Patiño on 05/02/2016.
 */
public interface UnitRepository {

    List<Unit> obtainUnit();

    void persistUnit(List<Unit> unitList) throws UniqueException, PersistException;

    void deleteUnit(int id);

    int countUnit();

    void clear();
}