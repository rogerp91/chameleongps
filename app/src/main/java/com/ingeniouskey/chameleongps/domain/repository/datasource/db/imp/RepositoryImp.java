package com.ingeniouskey.chameleongps.domain.repository.datasource.db.imp;

import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.AlertRepository;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.Repository;
import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.UnitRepository;

import javax.inject.Inject;

/**
 * Created by Roger Patiño on 11/02/2016.
 */
public class RepositoryImp implements Repository {

    UnitRepository unitRepository;
    AlertRepository alertRepository;

    @Inject
    public RepositoryImp(AlertRepository alertRepository, UnitRepository unitRepository) {
        this.alertRepository = alertRepository;
        this.unitRepository = unitRepository;
    }

    @Override
    public AlertRepository getAlert() {
        return alertRepository;
    }

    @Override
    public UnitRepository getUnit() {
        return unitRepository;
    }

}