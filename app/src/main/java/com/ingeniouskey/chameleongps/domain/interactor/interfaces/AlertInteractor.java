package com.ingeniouskey.chameleongps.domain.interactor.interfaces;

import com.ingeniouskey.chameleongps.mvp.model.Alert;

import java.util.List;

/**
 * Created by Roger Patiño on 23/12/2015.
 */
public interface AlertInteractor {

    interface Callback {

        void onSuccess(final List<Alert> listAlert);

        void onErrorConnection();

        void onErrorContect();

        void onErrorConnectionVoid();

        void onErrorContectVoid();

        void onError();

        void onError(String msgError);

    }

    void execute(boolean typeMethod, Callback callBackAnt);

}
