package com.ingeniouskey.chameleongps.domain.object.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Roger Patiño on 29/12/2015.
 */
public class UnitObject extends RealmObject {

    @PrimaryKey
    private int id;
    private double latitud;
    private double longitud;
    private String identificador;
    private boolean disponibilidad;
    private boolean encendido;
    private double velocidad;
    private int id_tipo_unidad;
    private String tipo;
    private String nombre;
    private String actualizacion;
    private boolean en_servicio;
    private int id_usuario;
    private String fecha_actual;

    public boolean isDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public int getId_tipo_unidad() {
        return id_tipo_unidad;
    }

    public void setId_tipo_unidad(int id_tipo_unidad) {
        this.id_tipo_unidad = id_tipo_unidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getActualizacion() {
        return actualizacion;
    }

    public void setActualizacion(String actualizacion) {
        this.actualizacion = actualizacion;
    }

    public boolean isEn_servicio() {
        return en_servicio;
    }

    public void setEn_servicio(boolean en_servicio) {
        this.en_servicio = en_servicio;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getFecha_actual() {
        return fecha_actual;
    }

    public void setFecha_actual(String fecha_actual) {
        this.fecha_actual = fecha_actual;
    }

}
