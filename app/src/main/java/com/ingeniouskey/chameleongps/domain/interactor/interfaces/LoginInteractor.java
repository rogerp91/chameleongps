package com.ingeniouskey.chameleongps.domain.interactor.interfaces;

/**
 * Created by Roger Patiño on 16/12/2015.
 */
public interface LoginInteractor {

    interface Callback {

        void onSuccess();

        void onError(String msgError);

        void onErrorData();

        void onErrorProfile();
    }

    void execute(String user, String password, Callback callBackAnt);

}