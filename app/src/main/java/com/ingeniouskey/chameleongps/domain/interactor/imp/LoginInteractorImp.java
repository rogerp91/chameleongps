package com.ingeniouskey.chameleongps.domain.interactor.imp;

import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ingeniouskey.chameleongps.R;
import com.ingeniouskey.chameleongps.UIApp;
import com.ingeniouskey.chameleongps.domain.interactor.interfaces.LoginInteractor;
import com.ingeniouskey.chameleongps.domain.repository.datasource.api.HttpRestClient;
import com.ingeniouskey.chameleongps.executor.Executor;
import com.ingeniouskey.chameleongps.executor.Interactor;
import com.ingeniouskey.chameleongps.executor.MainThread;
import com.ingeniouskey.chameleongps.mvp.model.Login;
import com.ingeniouskey.chameleongps.mvp.model.Profile;
import com.ingeniouskey.chameleongps.mvp.model.Token;
import com.ingeniouskey.chameleongps.utils.Constants;
import com.ingeniouskey.chameleongps.utils.Prefs;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roger Patiño on 16/12/2015.
 */
public class LoginInteractorImp implements Interactor, LoginInteractor {

    protected static String TAG = LoginInteractorImp.class.getSimpleName();

    private final Executor executor;
    private final MainThread mainThread;
    private LoginInteractor.Callback callback;

    private String user;
    private String password;

    GsonBuilder gsonBuilder = null;
    Gson gson = null;
    OkHttpClient client = null;
    Retrofit retrofit = null;
    HttpRestClient restClient = null;

    @Inject
    public LoginInteractorImp(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
    }

    @Override
    public void execute(String user, String password, Callback callBackAnt) {
        if (user == null || user.equals("")) {
            throw new IllegalArgumentException("Error user.");
        }

        if (password == null || password.equals("")) {
            throw new IllegalArgumentException("Error pass.");
        }

        if (callBackAnt == null) {
            throw new IllegalArgumentException("Callback must not be null or response would not be able to be notified.");
        }

        this.callback = callBackAnt;
        this.user = user;
        this.password = password;
        executor.run(this);
    }

    @Override
    public void run() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                gsonBuilder = new GsonBuilder();
                gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
                gson = gsonBuilder.create();
                client = new OkHttpClient();
                client.setReadTimeout(60, TimeUnit.SECONDS);
                client.setConnectTimeout(60, TimeUnit.SECONDS);
                retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson)).baseUrl(HttpRestClient.BASE_URL).client(client).build();
                requestPost();
            }
        });
    }

    public void requestPost() {//verificar el login servidor
        //Log.i(TAG, "requestPost");
        //Log.d(TAG, new Gson().toJson(new Login(user, password, "mobile")));
        restClient = retrofit.create(HttpRestClient.class);
        Call<Token> tokenCall = restClient.performLogin(new Login(user, password, "mobile"));
        tokenCall.enqueue(new retrofit.Callback<Token>() {
            @Override
            public void onResponse(Response<Token> response, Retrofit retrofit) {
                switch (response.code()) {
                    case 200:
                        System.out.println(response.body());
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        if (response.body() == null || response.body().getToken().equals("")) {
                            notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_204));
                        } else {
                            Token token = response.body();
                            Prefs.putString(Constants.TOKEN, token.getToken());
                            getProfile(token.getToken());
                        }
                        break;
                    case 204:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_204));
                        break;
                    case 400:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_400));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        System.out.println(response.body());
                        break;
                    case 401:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionErrorData();
                        break;
                    case 403:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_403));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 404:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_404));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 500:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_500));
                        System.out.println(response.body());
                        System.out.println(response.headers());
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    default:
                        notifyPetitionError(UIApp.getContext.getString(R.string.error_occurred));
                        break;
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(TAG, executor.toString());
                notifyPetitionError(t.toString());
            }
        });
    }

    private void getProfile(String token) {
        Call<Profile> profileCall = restClient.performProfile(HttpRestClient.PREFIX + token);
        profileCall.enqueue(new retrofit.Callback<Profile>() {
            @Override
            public void onResponse(Response<Profile> response, Retrofit retrofit) {
                System.out.println("onResponse: " + response.body());
                System.out.println("onResponse: " + response.body().getUsuario());
                switch (response.code()) {
                    case 200:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        if (response.body() != null || !response.body().equals("")) {
                            Profile profile = response.body();
                            Prefs.putString(Constants.USUARIO, profile.getUsuario());
                            Prefs.putString(Constants.CORREO, profile.getCorreo_electronico());
                            notifyPetitionSuccess();
                        } else {
                            notifyPetitionErrorProfile();
                        }
                        break;
                    case 400:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_400));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 401:
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        notifyPetitionErrorData();
                        break;
                    case 403:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_403));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 404:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_404));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    case 500:
                        notifyPetitionError(UIApp.getContext.getString(R.string.msg_http_500));
                        Log.d(TAG, "STATUS: " + Integer.toString(response.code()));
                        break;
                    default:
                        notifyPetitionError(UIApp.getContext.getString(R.string.error_occurred));

                }
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println(t.toString());
                notifyPetitionError(t.toString());
            }
        });
    }

    private void notifyPetitionError(final String msjError) {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onError(msjError);
            }
        });
    }

    private void notifyPetitionErrorData() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorData();
            }
        });
    }

    private void notifyPetitionErrorProfile() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorProfile();
            }
        });
    }

    private void notifyPetitionSuccess() {
        mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccess();
            }
        });
    }
}