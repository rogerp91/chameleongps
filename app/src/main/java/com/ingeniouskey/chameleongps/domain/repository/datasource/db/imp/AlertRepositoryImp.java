package com.ingeniouskey.chameleongps.domain.repository.datasource.db.imp;

import android.util.Log;

import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.AlertRepository;
import com.ingeniouskey.chameleongps.exception.PersistException;
import com.ingeniouskey.chameleongps.exception.UniqueException;
import com.ingeniouskey.chameleongps.mvp.model.Alert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 05/02/2016.
 */
public class AlertRepositoryImp implements AlertRepository {

    protected static String TAG = AlertRepositoryImp.class.getSimpleName();
    private Realm realm;

    @Inject
    public AlertRepositoryImp() {
        realm = Realm.getDefaultInstance();
    }

    @Override
    public List<Alert> obtainAlert() {
        Log.d(TAG, "obtainAlert");
        ArrayList<Alert> list = new ArrayList<>();
        RealmResults<Alert> realmResults = realm.where(Alert.class).findAll();
        for (Alert obj : realmResults) {
            list.add(obj);
        }
        //Log.d(TAG, "SIZE:" + Integer.toString(list.size()));
        return list;
    }

    @Override
    public void persistAlert(List<Alert> alertList) throws UniqueException, PersistException {
        try {
            for (Alert alert : alertList) {
                realm.beginTransaction();
                Alert alert1 = realm.createObject(Alert.class);
                alert1.setId(alert.getId());
                alert1.setTipo(alert.getTipo());
                alert1.setFecha_hora_enviado(alert.getFecha_hora_enviado());
                alert1.setLeido(alert.getLeido());
                alert1.setIdentificador(alert.getIdentificador());
                alert1.setLatitud(alert.getLatitud());
                alert1.setLongitud(alert.getLongitud());
                alert1.setVelocidad(alert.getVelocidad());
                alert1.setId_unidad(alert.getId_unidad());
                alert1.setId_perimetro(alert.getId_perimetro());
                alert1.setId_usuario(alert.getId_usuario());
                alert1.setFecha_actual(alert.getFecha_actual());
                realm.commitTransaction();
            }
        } catch (RealmException e) {
            new PersistException();
        } catch (RealmPrimaryKeyConstraintException e) {
            new UniqueException();
        } catch (RuntimeException e) {
            new UnknownError();
        }
    }

    @Override
    public void deleteAlert(int id) {
        synchronized (realm) {
            Log.d(TAG, "deleteAlert");
            realm.beginTransaction();
            Alert alert = realm.where(Alert.class).equalTo("id", id).findFirst();
            alert.removeFromRealm();
            realm.commitTransaction();
        }
    }

    @Override
    public void clear() {
        realm.beginTransaction();
        realm.clear(Alert.class);
        realm.commitTransaction();
    }

}