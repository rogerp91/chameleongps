package com.ingeniouskey.chameleongps.domain.repository.datasource.db.imp;

import android.util.Log;

import com.ingeniouskey.chameleongps.domain.repository.datasource.db.interfaces.UnitRepository;
import com.ingeniouskey.chameleongps.exception.PersistException;
import com.ingeniouskey.chameleongps.exception.UniqueException;
import com.ingeniouskey.chameleongps.mvp.model.Unit;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

/**
 * Created by Roger Patiño on 05/02/2016.
 */
public class UnitRepositoryImp implements UnitRepository {

    protected static String TAG = UnitRepositoryImp.class.getSimpleName();
    private Realm realm;

    @Inject
    public UnitRepositoryImp() {
        realm = Realm.getDefaultInstance();
    }


    @Override
    public void persistUnit(List<Unit> unitList) throws UniqueException, PersistException {
        synchronized (realm) {
            try {
                for (Unit unit : unitList) {
                    realm.beginTransaction();
                    Unit unitObject = realm.createObject(Unit.class);
                    unitObject.setId(unit.getId());
                    unitObject.setLatitud(unit.getLatitud());
                    unitObject.setLongitud(unit.getLongitud());
                    unitObject.setIdentificador(unit.getIdentificador());
                    unitObject.setDisponibilidad(unit.isDisponibilidad());
                    unitObject.setEncendido(unit.isEncendido());
                    unitObject.setVelocidad(unit.getVelocidad());
                    unitObject.setId_tipo_unidad(unit.getId_tipo_unidad());
                    unitObject.setTipo(unit.getTipo());
                    unitObject.setNombre(unit.getNombre());
                    unitObject.setActualizacion(unit.getActualizacion());
                    unitObject.setEn_servicio(unit.isEn_servicio());
                    unitObject.setId_usuario(unit.getId_usuario());
                    unitObject.setFecha_actual(unit.getFecha_actual());
                    realm.commitTransaction();
                }
            } catch (RealmException e) {
                new PersistException();
            } catch (RealmPrimaryKeyConstraintException e) {
                new UniqueException();
            } catch (RuntimeException e) {
                new UnknownError();
            }
        }
    }

    @Override
    public List<Unit> obtainUnit() {
        Log.d(TAG, "obtainUnit");
        synchronized (realm) {
            List<Unit> list = new ArrayList<>();
            RealmResults<Unit> realmResults = realm.where(Unit.class).findAll();
            for (Unit obj : realmResults) {
                if (obj.getId() != 0) {
                    list.add(obj);
                }
            }
            Log.d(TAG, "SIZE:" + Integer.toString(list.size()));
            return list;
        }
    }

    @Override
    public void deleteUnit(int id) {
        synchronized (realm) {
            Log.d(TAG, "deleteUnit");
            realm.beginTransaction();
            Unit unit = realm.where(Unit.class).equalTo("id", id).findFirst();
            unit.removeFromRealm();
            realm.commitTransaction();
        }
    }

    @Override
    public int countUnit() {
        Log.d(TAG, "countUnit");
        synchronized (realm) {
            List<Unit> list = new ArrayList<>();
            RealmResults<Unit> realmResults = realm.where(Unit.class).findAll();
            for (Unit obj : realmResults) {
                list.add(obj);
            }
            int cont = 0;
            for (Unit obj : list) {
                if (obj.isEncendido() == true) {
                    cont++;
                }
            }
            return cont;
        }
    }

    @Override
    public void clear() {
        realm.beginTransaction();
        realm.clear(Unit.class);
        realm.commitTransaction();
    }

}