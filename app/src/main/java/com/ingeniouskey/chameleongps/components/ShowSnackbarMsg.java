package com.ingeniouskey.chameleongps.components;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.ingeniouskey.chameleongps.mvp.view.GetView;
import com.ingeniouskey.chameleongps.mvp.presenter.interfaces.ShowMsgPresenter;

public class ShowSnackbarMsg implements ShowMsgPresenter {

    private GetView view;

    public ShowSnackbarMsg(GetView view) {
        this.view = view;
    }

    @Override
    public void showMsgShort(String error, int color) {
        Snackbar snackbar = Snackbar.make(view.getView(), error, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(color);
        snackbar.show();
    }

    @Override
    public void showMsgLong(String error, int color) {
        Snackbar snackbar = Snackbar.make(view.getView(), error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(color);
        snackbar.show();
    }
}